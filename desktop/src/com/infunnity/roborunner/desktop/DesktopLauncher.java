package com.infunnity.roborunner.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.infunnity.roborunner.RoboRunner;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.width = 1024;
        config.height = 576;
//
//        config.width = 800;
//        config.height = 480;

        config.width = 700;
        config.height = 420;

        // MAX FPS - 4.5k on empty project
        // 4.0k - ent engine, box2d, render + one entity
//        config.vSyncEnabled = false;
//        config.foregroundFPS = 0;
//        config.backgroundFPS = 0;

		new LwjglApplication(new RoboRunner(), config);
	}
}
