package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.infunnity.roborunner.RoboRunner;

/**
 * Created by Lukasz on 2014-09-25.
 */
public class ParticleSystem extends EntitySystem {

    private Array<ParticleEffectPool> effectPools = new Array();
    private Array<ParticleEffectPool.PooledEffect> runningEffects = new Array();

    /*
	 * Effects ids
	 */
    public static final short RUN_PARTICLE = 0;
    public static final short JUMP_PARTICLE = 1;
    public static final short SPARK_PARTICLE = 2;

    public ParticleSystem() {
        TextureAtlas atlas = RoboRunner.assets.get(RoboRunner.assets.PlayerAtlas, TextureAtlas.class);
        ParticleEffect temp = new ParticleEffect();

        // RUN_PARTICLE = 0
        temp = RoboRunner.assets.get(RoboRunner.assets.runParticle, ParticleEffect.class);
        effectPools.add(new ParticleEffectPool(temp, 30, 50));

        // JUMP_PARTICLE = 1
        temp = RoboRunner.assets.get(RoboRunner.assets.jumpParticle, ParticleEffect.class);
        effectPools.add(new ParticleEffectPool(temp, 30, 50));

        // SPARK_PARTICLE = 2
        temp = RoboRunner.assets.get(RoboRunner.assets.sparkParticle, ParticleEffect.class);
        effectPools.add(new ParticleEffectPool(temp, 30, 50));
    }

    @Override
    public void update(float deltaTime) {
//        super.update(deltaTime);

        for (int i = runningEffects.size - 1; i >= 0; i--) {
            ParticleEffectPool.PooledEffect effect = runningEffects.get(i);

            effect.update(deltaTime);

            if (effect.isComplete()) {
                effect.free();
                runningEffects.removeIndex(i);
            }
        }
    }

    public void draw(SpriteBatch batch) {
        for(ParticleEffect effect : runningEffects) {
            effect.draw(batch);
        }
    }

    /*
 * Reset all effects that are still alive
 */
    public void reset() {
        for (int i = runningEffects.size - 1; i >= 0; i--)
            runningEffects.get(i).free();
        runningEffects.clear();
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
    }

    /*
 * Obtain effect, which parameters can be changed.
 * Configured effect should be spawned by addEffect(effect);
 */
    public ParticleEffectPool.PooledEffect obtainEffect(short effectId) {
        return effectPools.get(effectId).obtain();
    }

    /*
     * Adds effect to render
     */
    public void addEffect(ParticleEffectPool.PooledEffect effect) {
        runningEffects.add(effect);
    }

}
