package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.infunnity.roborunner.components.entityComponents.OnGroundSensorComponent;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-09.
 */
public class OnGroundSensorSystem extends IteratingSystem {

    // Mappers
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<OnGroundSensorComponent> onGroundMap = ComponentMapper.getFor(OnGroundSensorComponent.class);
    private ComponentMapper<TransformComponent> transformMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);

    private PhysicsComponent physComp;
    private OnGroundSensorComponent onGroundComp;
    private TransformComponent playerTransformComp;
    private PlayerComponent playerComp;

    public OnGroundSensorSystem() {
        super(Family.getFor(PhysicsComponent.class, OnGroundSensorComponent.class));
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        // TODO: throw all out and connet sensor by Joint!

        physComp = physMap.get(entity);
        onGroundComp = onGroundMap.get(entity);

        playerTransformComp = transformMap.get(onGroundComp.player);
        playerComp = playerMap.get(onGroundComp.player);

        physComp.body.setLinearVelocity(0, 0);
        physComp.body.setTransform((playerTransformComp.pos.x) * Box2DWorld.WORLD_TO_BOX,
                (playerTransformComp.pos.y - playerComp.HEIGHT / 2) * Box2DWorld.WORLD_TO_BOX, 0);
    }
}
