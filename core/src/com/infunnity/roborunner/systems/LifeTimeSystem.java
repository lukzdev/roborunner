package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.infunnity.roborunner.components.LifeTimeComponent;
import com.infunnity.roborunner.components.TextureRegionComponent;
import com.infunnity.roborunner.components.TransformComponent;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class LifeTimeSystem extends IteratingSystem {

    private ComponentMapper<LifeTimeComponent> ltMap = ComponentMapper.getFor(LifeTimeComponent.class);


    public LifeTimeSystem() {
        super(Family.getFor(LifeTimeComponent.class));

    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        ltMap.get(entity).lifeTime += deltaTime;
    }

}
