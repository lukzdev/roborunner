package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.infunnity.roborunner.components.entityComponents.OnWallSensorComponent;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-09.
 */
public class OnWallSensorSystem extends IteratingSystem {

    // Mappers
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<OnWallSensorComponent> onWallMap = ComponentMapper.getFor(OnWallSensorComponent.class);
    private ComponentMapper<TransformComponent> transformMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);

    private PhysicsComponent physComp;
    private OnWallSensorComponent onWallComp;
    private TransformComponent playerTransformComp;
    private PlayerComponent playerComp;

    public OnWallSensorSystem() {
        super(Family.getFor(PhysicsComponent.class, OnWallSensorComponent.class));
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        // TODO: throw all out and connet sensor by Joint!

        physComp = physMap.get(entity);
        onWallComp = onWallMap.get(entity);

        playerTransformComp = transformMap.get(onWallComp.player);
        playerComp = playerMap.get(onWallComp.player);

        physComp.body.setLinearVelocity(0, 0);

        if(onWallComp.left) {
            physComp.body.setTransform((playerTransformComp.pos.x - playerComp.WIDTH / 2) * Box2DWorld.WORLD_TO_BOX,
                    (playerTransformComp.pos.y) * Box2DWorld.WORLD_TO_BOX, 0);
        } else {
            physComp.body.setTransform((playerTransformComp.pos.x + playerComp.WIDTH / 2) * Box2DWorld.WORLD_TO_BOX,
                    (playerTransformComp.pos.y) * Box2DWorld.WORLD_TO_BOX, 0);
        }

    }
}
