package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.components.GuiComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.controllers.PlayerInputHandler;

/**
 * Created by Lukasz on 2014-09-15.
 */
public class GuiSystem extends IteratingSystem {

    private ComponentMapper<GuiComponent> guiMap = ComponentMapper.getFor(GuiComponent.class);
    private GuiComponent guiComp;

    private PlayerComponent playerComp;
    private PlayerInputHandler inputHandler;

    public GuiSystem(Entity player, PlayerInputHandler inputHandler) {
        super(Family.getFor(GuiComponent.class));

        playerComp = ComponentMapper.getFor(PlayerComponent.class).get(player);
        this.inputHandler = inputHandler;
    }

    private Vector2 tempSliderPos = new Vector2(0,0);
    private Vector2 tempOldSliderPos = new Vector2(0,0);

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        guiComp = guiMap.get(entity);
        if(inputHandler.getSpeedSliderTouchID() != -1) {
            tempSliderPos.set(inputHandler.getSpeedSliderPos());
            tempSliderPos = guiComp.stage.screenToStageCoordinates(tempSliderPos);

            tempOldSliderPos.set(guiComp.movementSlider.getCenterX(), guiComp.movementSlider.getCenterY());
            tempOldSliderPos.lerp(tempSliderPos, deltaTime * 10);

            guiComp.movementSlider.setCenterPosition(tempOldSliderPos.x, tempOldSliderPos.y);
            guiComp.movementSlider.setVisible(true);

            guiComp.movementSlider.setValue((inputHandler.getSliderMoveCm()) * 100);

            if(Math.abs(inputHandler.getSliderMoveCm()) >= 0.31f) {
                guiComp.movementSlider.setColor(Color.RED);
            } else {
                guiComp.movementSlider.setColor(Color.WHITE);
            }


        } else {
            guiComp.movementSlider.setVisible(false);
            guiComp.movementSlider.setValue(50);
        }

        guiComp.stage.act(deltaTime);

    }



}
