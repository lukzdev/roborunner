package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.models.Map;

import java.util.Comparator;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class RenderingSystem extends IteratingSystem {

    private Array<Entity> beforeRenderQueue;
    private Array<Entity> renderQueue;
    private Array<Entity> afterRenderQueue;

    private Comparator<Entity> comparator;

//    private OrthographicCamera cam;
    private SpriteBatch batch;

    private Map map;
    private OrthogonalTiledMapRenderer mapRenderer;

//    private Texture tempBg;

    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<TextureRegionComponent> trMap = ComponentMapper.getFor(TextureRegionComponent.class);
    private ComponentMapper<MultipleTextureRegionComponent> mulTrMap = ComponentMapper.getFor(MultipleTextureRegionComponent.class);
    private ComponentMapper<CameraComponent> camMap = ComponentMapper.getFor(CameraComponent.class);
    private ComponentMapper<MapComponent> mapMap = ComponentMapper.getFor(MapComponent.class);

    private MapComponent mapCom;
    private GuiComponent guiComp;
    private TextureRegionComponent trComp;
    private CameraComponent camCom;

    private Texture bg;
    private Entity camEntity;

    private ParticleSystem particleSystem;

    public RenderingSystem(Map map, Entity guiEntity, int priority, Entity camera) {
        super(Family.getFor(ComponentType.getBitsFor(TransformComponent.class),
                ComponentType.getBitsFor(MultipleTextureRegionComponent.class, TextureRegionComponent.class),
                ComponentType.getBitsFor()), priority);

        bg = new Texture(Gdx.files.internal("bgs/bg.png"));
        bg.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        bg.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);

        renderQueue = new Array<Entity>();
        beforeRenderQueue = new Array<Entity>();
        afterRenderQueue = new Array<Entity>();

        comparator = new Comparator<Entity>() {
            @Override
            public int compare(Entity entityA, Entity entityB) {
                return (int)Math.signum(tMap.get(entityB).pos.z -
                        tMap.get(entityA).pos.z);
            }
        };

        this.camEntity = camera;
        this.camCom = camMap.get(camEntity);
        this.guiComp = ComponentMapper.getFor(GuiComponent.class).get(guiEntity);

        batch = new SpriteBatch();
        batch.setProjectionMatrix(camCom.camera.combined);

        this.map = map;
        mapRenderer = new OrthogonalTiledMapRenderer(map.getMap(),batch);
        mapRenderer.setView(camCom.camera);


        mapCom = mapMap.get(map);
//        tempBg = RoboRunner.assets.get(RoboRunner.assets.TempBG, Texture.class);

//        setProcessing(false);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        particleSystem = engine.getSystem(ParticleSystem.class);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

//        renderQueue.sort(comparator);
        mapRenderer.setView(camCom.camera);

        Gdx.gl.glClearColor(0, 0, 0, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camCom.camera.combined);
        batch.begin();
        batch.draw(bg,
                camCom.camera.position.x - (camCom.camera.viewportWidth * camCom.camera.zoom) / 2,
                camCom.camera.position.y - (camCom.camera.viewportHeight * camCom.camera.zoom) / 2,

                camCom.camera.viewportWidth * camCom.camera.zoom,
                camCom.camera.viewportHeight * camCom.camera.zoom,
                (int)(camCom.camera.position.x - (camCom.camera.viewportWidth * camCom.camera.zoom) / 2),
                (int)(camCom.camera.position.y - (camCom.camera.viewportHeight * camCom.camera.zoom) / 2),

                (int)(RoboRunner.TARGET_WIDTH * camCom.camera.zoom),
                (int)(RoboRunner.TARGET_HEIGHT * camCom.camera.zoom), false, true);
//        batch.draw(tempBg, 0, 0);

        batch.setColor(mapCom.backgroundTilesColor);
        mapRenderer.renderTileLayer(mapCom.backgroundTiles);

        batch.setColor(Color.WHITE);
        renderQueue(beforeRenderQueue, batch);

        particleSystem.draw(batch);

        batch.setColor(mapCom.groundTilesColor);
        mapRenderer.renderTileLayer(mapCom.groundTiles);

        batch.setColor(Color.WHITE);
        renderQueue(renderQueue, batch);

        // Cracks!
        mapRenderer.renderTileLayer(mapCom.crackTiles);

        batch.setColor(mapCom.foregroundTilesColor);
        mapRenderer.renderTileLayer(mapCom.foregroundTiles);

        batch.setColor(Color.WHITE);
        renderQueue(afterRenderQueue, batch);

//        batch.setColor(map.getForegroundTilesColor());
//        mapRenderer.renderTileLayer(map.getForegroundTiles());

        batch.end();
        guiComp.stage.draw();

        renderQueue.clear();
        afterRenderQueue.clear();
        beforeRenderQueue.clear();
    }

    // Helpers
    private float width;
    private float height;
    private float originX;
    private float originY;
    private TextureRegionComponent tex;
    private TransformComponent t;
    private MultipleTextureRegionComponent mulTr;
    private Rectangle textureRectangle = new Rectangle();
    public void renderQueue(Array<Entity> queue, SpriteBatch batch) {
        for (Entity entity : queue) {
            tex = trMap.get(entity);
            t = tMap.get(entity);
            mulTr = mulTrMap.get(entity);

            // Render MulitTextureRegion
            if(mulTr != null) {
                width = mulTr.textureRegion.getRegionWidth();
                height = mulTr.textureRegion.getRegionHeight();

                originX = width * 0.5f;
                originY = height * 0.5f;

                if(mulTr.xDirection) {
                    originX = width * mulTr.repeat *  0.5f;
                } else {
                    originY = height * mulTr.repeat * 0.5f;
                }

                float widthDirected = width;
                float heightDirected = height;

                // TODO: FUCKING UGLY!!!!
                if(t.rotation == 180) {
                    widthDirected = -width;
                }

                if(t.rotation == 270) {
                    heightDirected = -height;
                }

                for(int i = 0; i < mulTr.repeat; i++) {

                    if(mulTr.xDirection) {
                        batch.draw(mulTr.textureRegion,
                                (t.pos.x - originX) + widthDirected * i, t.pos.y - originY,
                                originX, originY,
                                width, height,
                                t.scale.x, t.scale.y,
                                t.rotation);
                    } else {
                        batch.draw(mulTr.textureRegion,
                                t.pos.x - originX - heightDirected, (t.pos.y - originY) + widthDirected * i - width,
                                originX, originY,
                                width, height,
                                t.scale.x, t.scale.y,
                                t.rotation);
                    }
                }
                mulTr = null;
                continue;
            }

            // Render TextureRegion
            width = tex.textureRegion.getRegionWidth();
            height = tex.textureRegion.getRegionHeight();
            if(!tex.originSet) {
                originX = width * 0.5f;
                originY = height * 0.5f;
            } else {
                originX = tex.originX;
                originY = tex.originY;
            }

            textureRectangle.set(t.pos.x - originX + tex.offsetX, t.pos.y - originY + tex.offsetY, width, height);

            // Culling
            if(!camCom.actualBounding.overlaps(textureRectangle)) {
                continue;
            }

            batch.draw(tex.textureRegion,
                    textureRectangle.x, textureRectangle.y,
                    originX, originY,
                    width, height,
                    t.scale.x, t.scale.y,
                    t.rotation);
        }
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        trComp = trMap.get(entity);
        mulTr = mulTrMap.get(entity);

        if(mulTr != null) {
            trComp = (TextureRegionComponent)mulTr;
        }

//        if(trComp.textureRegion)

        if(!trComp.isVisible) return;

        if(trComp.layer == 1) {
            afterRenderQueue.add(entity);
        } else if(trComp.layer == -1) {
            beforeRenderQueue.add(entity);
        } else {
            renderQueue.add(entity);
        }


    }

    public Entity getCamEntity() {
        return camEntity;
    }
}
