package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.TransformComponent;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class Box2DTransformSystem extends IteratingSystem {

    public static float scale;

    public Box2DTransformSystem(float scale, int priority) {
        super(Family.getFor(TransformComponent.class, PhysicsComponent.class), priority);
        this.scale = scale;
    }

    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PhysicsComponent> pMap = ComponentMapper.getFor(PhysicsComponent.class);
    private TransformComponent t;
    private PhysicsComponent p;
    private Vector2 tempVec2;

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        p = pMap.get(entity);
        if(!(p.bodyType == BodyDef.BodyType.DynamicBody || p.bodyType == BodyDef.BodyType.KinematicBody))
            return;

        t = tMap.get(entity);

        tempVec2 = p.body.getPosition();
        t.pos.x = tempVec2.x * scale;
        t.pos.y = tempVec2.y * scale;

        t.rotation = p.body.getAngle() * MathUtils.radDeg;
    }
}
