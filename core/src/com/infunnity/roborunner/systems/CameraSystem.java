package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.infunnity.roborunner.components.CameraComponent;
import com.infunnity.roborunner.components.TransformComponent;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class CameraSystem extends IteratingSystem {
    private boolean pause = false;

    private ComponentMapper<TransformComponent> transMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<CameraComponent> camMap = ComponentMapper.getFor(CameraComponent.class);
    private CameraComponent camComp;
    private TransformComponent transComp;

    public CameraSystem() {
        super(Family.getFor(CameraComponent.class));
    }

    private Vector3 newCamPos = new Vector3(0, 0, 0);

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        camComp = camMap.get(entity);
        transComp = transMap.get(camComp.target);

//        camComp.camera.position.set(camComp.camera.zoom * RoboRunner.TARGET_WIDTH / 2,
//                camComp.camera.zoom * RoboRunner.TARGET_HEIGHT / 2, 0);

        newCamPos.x = MathUtils.clamp(transComp.pos.x, camComp.bounding.x + (camComp.camera.viewportWidth * camComp.camera.zoom) / 2,
                camComp.bounding.width - (camComp.camera.viewportWidth * camComp.camera.zoom) / 2);

        newCamPos.y = MathUtils.clamp(transComp.pos.y, camComp.bounding.y + (camComp.camera.viewportHeight * camComp.camera.zoom) / 2,
                camComp.bounding.height - (camComp.camera.viewportHeight * camComp.camera.zoom) / 2);

        // Camera shake
        if(camComp.shakeTime > 0) {
            camComp.shakeTime -= deltaTime;
            newCamPos.x += MathUtils.random(-5 * camComp.shakeMagnitude, 5 * camComp.shakeMagnitude);
            newCamPos.y += MathUtils.random(-5 * camComp.shakeMagnitude, 5 * camComp.shakeMagnitude);
        } else {
            // TODO: Ugly reset
            camComp.shakeMagnitude = 1;
        }

//        camComp.camera.position.lerp(newCamPos, 25 * deltaTime);
        camComp.camera.position.set(MathUtils.round(newCamPos.x), MathUtils.round(newCamPos.y), 0);

        camComp.camera.update();

//        CameraComponent cam = entity.getComponent(CameraComponent.class);
//
//        if (cam.target == null) {
//            return;
//        }
//
//        TransformComponent target = cam.target.getComponent(TransformComponent.class);
//
//        if (target == null) {
//            return;
//        }
//
//        cam.camera.position.y = Math.max(cam.camera.position.y, target.pos.y);

        camComp.actualBounding.set((int)Math.floor(camComp.camera.position.x - camComp.camera.viewportWidth / 2 * camComp.camera.zoom),
                (int)Math.floor(camComp.camera.position.y - camComp.camera.viewportHeight / 2 * camComp.camera.zoom),
                (int)Math.ceil(camComp.camera.viewportWidth * camComp.camera.zoom), (int)Math.ceil(camComp.camera.viewportHeight * camComp.camera.zoom));

    }

    @Override
    public boolean checkProcessing() {
        return !pause;
    }

    public void pause(boolean pause) {
        this.pause = pause;
    }
}
