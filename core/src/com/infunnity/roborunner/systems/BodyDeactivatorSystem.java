package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.infunnity.roborunner.components.BodyDeactivatorComponent;
import com.infunnity.roborunner.components.PhysicsComponent;

/**
 * Created by Lukasz on 2014-09-19.
 */
public class BodyDeactivatorSystem extends IteratingSystem {

    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);

    public BodyDeactivatorSystem() {
        super(Family.getFor(BodyDeactivatorComponent.class, PhysicsComponent.class));
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        physMap.get(entity).body.setActive(false);
        entity.remove(BodyDeactivatorComponent.class);
    }
}
