package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.*;
import com.badlogic.gdx.physics.box2d.Body;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.models.Box2DWorld;
import com.infunnity.roborunner.models.entities.Player;
import com.infunnity.roborunner.systems.entitySystems.PlayerSystem;

/**
 * Created by Lukasz on 2014-09-19.
 */
public class MapSystem extends EntitySystem {

    private ComponentMapper<TextureRegionComponent> playerTexMap = ComponentMapper.getFor(TextureRegionComponent.class);
    private ComponentMapper<TransformComponent> playerTransMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<MapComponent> mapMap = ComponentMapper.getFor(MapComponent.class);

    private MapComponent mapComp;
    private PlayerComponent playerComp;
    private PhysicsComponent playerPhysicComp;
    private TransformComponent playerTransComp;

    private Player playerEntity;

    private Engine engine;

    public MapSystem() {
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        playerEntity = (Player)engine.getEntitiesFor(Family.getFor(PlayerComponent.class)).first();

        playerComp = playerMap.get(playerEntity);
        playerTransComp = playerTransMap.get(playerEntity);
        playerPhysicComp = physMap.get(playerEntity);

        mapComp = mapMap.get(engine.getEntitiesFor(Family.getFor(MapComponent.class)).first());

        this.engine = engine;
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        // Check dead counter
        if(playerComp.isDead) {
            checkRestartTime(deltaTime);

            if(playerEntity.pbp.toActivate) {
                playerEntity.pbp.activate();
            }

        }

        if(playerEntity.pbp.toDeactivate) {
            playerEntity.pbp.deactivate();
        }


        // Check if player outside map
        if(!playerComp.isDead && (playerTransComp.pos.y < mapComp.mapBounds.y ||
                playerTransComp.pos.x < mapComp.mapBounds.x ||
                playerTransComp.pos.x > mapComp.mapBounds.x + mapComp.mapBounds.width)) {
            killPlayer();
        }

    }

    public void checkRestartTime(float deltaTime) {
        mapComp.deadTime += deltaTime;
        if(mapComp.deadTime >= mapComp.MAX_DEAD_TIME) {
            restartMap();
        }
    }

    public void restartMap() {
        mapComp.deadTime = 0;
        playerEntity.pbp.toDeactivate = true;
        engine.getSystem(PlayerSystem.class).restartPlayer(playerEntity);
        engine.getSystem(ParticleSystem.class).reset();
        engine.getSystem(ResetSytem.class).setProcessing(true);
//        engine.getEntitiesFor()
    }

    public void killPlayer() {
        Body playerBody = physMap.get(playerEntity).body;

        playerEntity.pbp.reposition(playerBody.getPosition().x * Box2DWorld.BOX_TO_WORLD, playerBody.getPosition().y * Box2DWorld.BOX_TO_WORLD,
                playerBody.getLinearVelocity().x, playerBody.getLinearVelocity().x);
        playerEntity.pbp.toActivate = true;

        playerEntity.add(new BodyDeactivatorComponent());
        playerComp.isDead = true;
        playerTexMap.get(playerEntity).isVisible = false;
        engine.getSystem(PlayerSystem.class).pause(true);
    }
}
