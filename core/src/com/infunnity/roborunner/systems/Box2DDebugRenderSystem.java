package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by Lukasz on 2014-08-06.
 */
public class Box2DDebugRenderSystem extends EntitySystem {

    private World world;
    private static float SCALE;
    private Camera cam;

    private Box2DDebugRenderer renderer = new Box2DDebugRenderer(true, true, false, true, true, true);

    public Box2DDebugRenderSystem(World world, Camera cam, float scale, int priority) {
        super(priority);

        this.world = world;
        this.SCALE = scale;
        this.cam = cam;
    }

    @Override
    public void addedToEngine(Engine engine) {

    }

    @Override
    public void update(float deltaTime) {
        renderer.render(world, cam.combined.cpy().scl(SCALE));
    }

}
