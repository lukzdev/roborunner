package com.infunnity.roborunner.systems.entitySystems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.RotatingSawComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class RotatingSawSystem extends IteratingSystem {

    private ComponentMapper<RotatingSawComponent> rotSawMap = ComponentMapper.getFor(RotatingSawComponent.class);
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<RotatingSawComponent> sawMap = ComponentMapper.getFor(RotatingSawComponent.class);

    private PhysicsComponent physComp;
    private RotatingSawComponent rotSawComp;
    private TransformComponent tComp;

    public RotatingSawSystem() {
        super(Family.getFor(TransformComponent.class, PhysicsComponent.class, RotatingSawComponent.class));
    }


    Vector2 tempPos = new Vector2(0, 0);
    @Override
    public void processEntity(Entity entity, float deltaTime) {
        physComp = physMap.get(entity);
        rotSawComp = rotSawMap.get(entity);
        tComp = tMap.get(entity);

        // rotate blade
        tComp.rotation += deltaTime * sawMap.get(entity).spriteRotationSpeed * 360;
        tComp.rotation = tComp.rotation % 360;

        // rotate saw
        rotSawComp.sawRotation += deltaTime * 360 / rotSawComp.rotationSpeed;
        rotSawComp.sawRotation = rotSawComp.sawRotation % 360;

        // Rotating around point: http://matematyka.pisz.pl/strona/896.html
        tempPos.x = rotSawComp.rotationRadius * MathUtils.cosDeg(rotSawComp.sawRotation);
        tempPos.y = rotSawComp.rotationRadius * MathUtils.sinDeg(rotSawComp.sawRotation);

        tempPos.add(rotSawComp.startPos);

        // set new pos
        physComp.body.setTransform(tempPos.x * Box2DWorld.WORLD_TO_BOX, tempPos.y * Box2DWorld.WORLD_TO_BOX, 0);
        tComp.pos.set(tempPos.x, tempPos.y, tComp.pos.z);

    }
}
