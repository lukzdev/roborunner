package com.infunnity.roborunner.systems.entitySystems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.MapComponent;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.components.TextureRegionComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.models.Box2DWorld;
import com.infunnity.roborunner.models.Map;
import com.infunnity.roborunner.models.entities.Player;
import com.infunnity.roborunner.systems.ParticleSystem;
import com.infunnity.roborunner.utils.dermetfan.ArrayUtils;

/**
 * Created by Lukasz on 2014-08-05.
 */
public class PlayerSystem extends IteratingSystem {
    private boolean pause = false;

    // Mappers
    private ComponentMapper<TransformComponent> transformMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<TextureRegionComponent> texRegMap = ComponentMapper.getFor(TextureRegionComponent.class);
    private TransformComponent tComp;
    private PhysicsComponent physComp;
    private PlayerComponent playerComp;
    private TextureRegionComponent texRegComp;

    // Statics
    private final static float normMaxHorizSpeed = 7.5f;
    private final static float boostMaxHorizSpeed = 12;

    private final static float normalAccel = 0.21f; // 0.58 to full speed
    private final static float boostAccel = 0.53f; // 0.367 to max speed

    // TurnMuls also affect wall jumping
    private final static float normTurnMul = 2.0f;
    private final static float boostTurnMul = 1.2f;

    private final static float maxFallingVelocity = -6.5f;

    public final static float wallStickTime = 0.3f;
    private final static float wallJumpScalar = 13.5f;
    private Vector2 leftWallJump = new Vector2(1,1);
    private Vector2 rightWallJump = new Vector2(1,1);

    private Animation idle;
//    private Animation start;
    private TextureRegion wallSlide;
    private TextureRegion turn;
    private TextureRegion stopping;
    private Animation run;
    private Animation jump;

    private Map map;

    private ParticleSystem particleSystem;

    private Engine engine;

    private Vector2 lastNonZeroVelocity = new Vector2(0,0);

    public PlayerSystem() {
        super(Family.getFor(PlayerComponent.class, PhysicsComponent.class));
        leftWallJump.setAngle(45).nor().scl(wallJumpScalar);
        rightWallJump.setAngle(135).nor().scl(wallJumpScalar);

        TextureAtlas playerAtlas = RoboRunner.assets.get(RoboRunner.assets.PlayerAtlas, TextureAtlas.class);
        idle = new Animation(0.18f, playerAtlas.findRegions("idle"), Animation.PlayMode.LOOP);
//        wallSlide = new Animation(0.06f, playerAtlas.findRegions("wallhang"), Animation.PlayMode.NORMAL);
        wallSlide = playerAtlas.findRegion("wallhang");
        turn = playerAtlas.findRegion("run_change");
        stopping = playerAtlas.findRegions("slide").first();
        jump = new Animation(0.06f, playerAtlas.findRegions("jump"), Animation.PlayMode.NORMAL);
        run = new Animation(0.08f, playerAtlas.findRegions("run"), Animation.PlayMode.LOOP);
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        this.engine = engine;
        map = (Map)engine.getEntitiesFor(Family.getFor(MapComponent.class)).first();
    }


    private Vector2 velocity = new Vector2(0, 0);

//    float lastPosX = 0;
//    float latPosCounter = 0;
    @Override
    public void processEntity(Entity entity, float deltaTime) {
        physComp = physMap.get(entity);
        playerComp = playerMap.get(entity);
        tComp = transformMap.get(entity);

        // TODO: I don't see other way of getting that one :(
        if(particleSystem == null) {
            particleSystem = engine.getSystem(ParticleSystem.class);
        }

        // Allow wall jumps to be higher then normal
        gravityScaleCheck();

        velocity.set(physComp.body.getLinearVelocity());
        lastNonZeroVelocity.x = velocity.x != 0 ? velocity.x : lastNonZeroVelocity.x;
        lastNonZeroVelocity.y = velocity.y != 0 ? velocity.y : lastNonZeroVelocity.y;

        if(velocity.y > 0) {
            playerComp.isGoingUp = true;
        } else {
            playerComp.isGoingUp = false;
        }

        // Max out falling velocity
        if(velocity.y < maxFallingVelocity) {
            physComp.body.setLinearVelocity(velocity.x, maxFallingVelocity);
        }

        // Variable jump lenght
        if(playerComp.isGoingUp && !playerComp.jumpButton) {
            physComp.body.setLinearVelocity(velocity.x, velocity.y * deltaTime * 40);
        }

        if(playerComp.onGround > 0 && playerComp.jumpButton && !playerComp.isGoingUp && !playerComp.jumpConsumed) {
            // Hack!
            physComp.body.setLinearVelocity(velocity.x, 0);
            physComp.body.applyLinearImpulse(new Vector2(0, 2f), physComp.body.getWorldCenter(), true);
//            physComp.body.setLinearVelocity(velocity.x, 2);

            playerComp.isGoingUp = true;
            playerComp.jumpConsumed = true;
        }

        if((playerComp.rightWall > 0 || playerComp.leftWall > 0) && playerComp.jumpButton && playerComp.onGround == 0 && !playerComp.jumpConsumed) {
            if(playerComp.leftWall > 0) {
                physComp.body.setLinearVelocity(leftWallJump);
            } else {
                physComp.body.setLinearVelocity(rightWallJump);
            }
            playerComp.jumpConsumed = true;
        }
        velocity.set(physComp.body.getLinearVelocity());

        if(playerComp.onGround > 0 && !playerComp.isGoingUp) {
            // ON GROUND
            if(!walk(deltaTime)) {
                // Slow down if RIGHT of LEFT not pressed
                velocity.x = velocity.x * (60 - 10) * deltaTime;
            }
        } else {
            // IN AIR
            if((playerComp.rightWall > 0 || playerComp.leftWall > 0) && (playerComp.rightButton || playerComp.leftButton)) {
                playerComp.wallStickCountdown -= deltaTime;

                if(playerComp.wallStickCountdown < 0) {
                    walk(deltaTime);
                }
            } else {
                walk(deltaTime);
            }

        }

        // Clamp velocity
        velocity.x = MathUtils.clamp(velocity.x, -maxHorizSpeed(), maxHorizSpeed());

        // physComp.body.setLinearVelocity(velocity);

        // Second way
        float velChange = velocity.x - physComp.body.getLinearVelocity().x;
        float impulse = physComp.body.getMass() * velChange; //disregard time factor
        physComp.body.applyLinearImpulse(new Vector2(impulse, 0), physComp.body.getWorldCenter(), true);

        // Animation
        animatePlayer(entity, deltaTime);

        // If on ground, maybe you want to make some tiny cracks? (^.o)
        if(playerComp.groundContactPoint.size > 0) {
            Vector2[] contactPoints = playerComp.groundContactPoint.peek().getWorldManifold().getPoints();
            if(contactPoints.length == 2) {
                map.findTilesToCrack(contactPoints[0].scl(Box2DWorld.BOX_TO_WORLD), contactPoints[1].scl(Box2DWorld.BOX_TO_WORLD),
                        physComp.body.getPosition().scl(Box2DWorld.BOX_TO_WORLD), true);
            }
        }
    }

    // Used to big wall jumps
    public void gravityScaleCheck() {
        if(playerComp.rightWall > 0 || playerComp.leftWall > 0) {
            physComp.body.setGravityScale(0.66f);
        } else {
            physComp.body.setGravityScale(1f);
        }
    }

    // Temps
    private int sign = 0;
    private int currSign = 0;
    private float horizSpeed = 0;

    private boolean walk(float deltaTime) {
        // Walk
        if (playerComp.rightButton) sign = 1;
        else if (playerComp.leftButton) sign = -1;
        else return false;

        currSign = signOf(physComp.body.getLinearVelocity().x);
        horizSpeed = walkAccel();

        if ((currSign != 0) && (currSign != sign)) {
            horizSpeed *= turnMul();
        }

        velocity.x += horizSpeed * sign * deltaTime * 60;
        return true;
    }

    public void restartPlayer(Entity playerEnt) {
        physComp = physMap.get(playerEnt);
        playerComp = playerMap.get(playerEnt);

        playerComp.state = PlayerComponent.State.ON_GROUND;
        playerComp.isDead = false;
        playerComp.isGoingUp = false;
        playerComp.groundContactPoint.clear();

        tComp.pos.set(playerComp.startPos, 0);
        physComp.body.setTransform(playerComp.startPos.x * Box2DWorld.WORLD_TO_BOX, playerComp.startPos.y * Box2DWorld.WORLD_TO_BOX, 0);
        physComp.body.setLinearVelocity(0, 0);
        physComp.body.setActive(true);

        this.pause = false;
        texRegMap.get(playerEnt).isVisible = true;
    }

    public float animationTime = 0;
    public float onGroundAnimationTime = 0;

    public float runParticleShoot = 0;
    public void animatePlayer(Entity playerEnt, float deltaTime) {
        texRegComp = texRegMap.get(playerEnt);

        if(playerComp.onGround != 0) {
            // Variable animation speed
//            animationTime += deltaTime / 8 * Math.abs(velocity.x);
            onGroundAnimationTime += deltaTime;

            // Running
            if(Math.abs(velocity.x) > 2f || playerComp.rightButton || playerComp.leftButton) {
                // Variable animation speed
                animationTime += deltaTime / 8 * Math.abs(velocity.x);
                runParticleShoot += deltaTime;

                texRegComp.textureRegion = run.getKeyFrame(animationTime);

                // Particle shoot!
                if(runParticleShoot > 0.05f) {

//                    for(int i = 0; i < 5; i++) {
                        ParticleEffectPool.PooledEffect runParticle = particleSystem.obtainEffect(ParticleSystem.RUN_PARTICLE);
                        runParticle.setPosition(tComp.pos.x, tComp.pos.y - PlayerComponent.HEIGHT / 2);
                        particleSystem.addEffect(runParticle);
//                    }
                    runParticleShoot = 0;
                }

                // Changing turn?
                if ((currSign != 0) && (currSign != sign)) {
                    texRegComp.textureRegion = turn;
                }
            } else {
                // Variable animation speed
                animationTime += deltaTime;
                runParticleShoot = 0;

                if(Math.abs(velocity.x) > 0.1f ) {
                    // Stopping
                    texRegComp.textureRegion = stopping;
                } else {
                    // Stanging - Idle
                    texRegComp.textureRegion = idle.getKeyFrame(animationTime);
                }
            }

            // Landing
            if(onGroundAnimationTime < 0.15f && Math.abs(velocity.x) < 7) {
                texRegComp.textureRegion = jump.getKeyFrames()[0];
            }

        } else {
            animationTime += deltaTime;
            onGroundAnimationTime = 0;
            runParticleShoot = 0;

            // Jumping
            if(!playerComp.rightButton && !playerComp.leftButton && texRegComp.textureRegion == wallSlide) {
                sign = signOf(physComp.body.getLinearVelocity().x);
            }

            if(playerComp.isGoingUp) {
                texRegComp.textureRegion = jump.getKeyFrames()[1];
            } else {
                texRegComp.textureRegion = jump.getKeyFrames()[2];
            }

            // Check on wall
            if(playerComp.rightWall > 0) {
                texRegComp.textureRegion = wallSlide;
                sign = 1;
            }

            if(playerComp.leftWall > 0) {
                texRegComp.textureRegion = wallSlide;
                sign = -1;
            }
        }

//        // Jumping
//        if(playerComp.onGround == 0) {
//        }

        if((sign < 0 && !texRegComp.textureRegion.isFlipX()) || (sign > 0 && texRegComp.textureRegion.isFlipX())) {
            texRegComp.textureRegion.flip(true, false);
        }

    }

    float particleImpact = 0;
    float temp = 0;

    int startingMaxParticleCount = 0;
    float startingMaxVelocity = 0;
    float startingMinVelocity = 0;
    public void jumpParticles() {
        // Not ready yet!
        if(particleSystem == null)
            return;

        ParticleEffectPool.PooledEffect runParticle = particleSystem.obtainEffect(ParticleSystem.JUMP_PARTICLE);

        particleImpact = (Math.abs(lastNonZeroVelocity.y) * 1.5f) / 10; // bringing value to 0 - 1

        for (ParticleEmitter emitter : runParticle.getEmitters()) {

            // Particle count
            if(startingMaxParticleCount == 0) startingMaxParticleCount = emitter.getMaxParticleCount();
            temp = startingMaxParticleCount * particleImpact;
            emitter.setMaxParticleCount(Math.round(temp));

            // Velocity
            if(startingMaxVelocity == 0) startingMaxVelocity = emitter.getVelocity().getHighMax();
            if(startingMinVelocity == 0) startingMinVelocity = emitter.getVelocity().getHighMin();
            temp = startingMaxVelocity * particleImpact;
            emitter.getVelocity().setHighMax(Math.max(temp, 275));
            temp = startingMinVelocity * particleImpact;
            emitter.getVelocity().setHighMin(Math.max(temp, 200));
        }

        runParticle.setPosition(tComp.pos.x - PlayerComponent.WIDTH / 2, tComp.pos.y - PlayerComponent.HEIGHT / 2 - 10);
        particleSystem.addEffect(runParticle);
    }

    private int signOf(float x) {
        if(x > 0) return 1;
            else return -1;
    }

    float turnMul() {
        if (playerComp.boostButton) return boostTurnMul;
        return normTurnMul;
    }

    float maxHorizSpeed() {
        if (playerComp.boostButton) return boostMaxHorizSpeed;
        return normMaxHorizSpeed;
    }

    float walkAccel() {
        if (playerComp.boostButton) return boostAccel;
        return normalAccel;
    }

    @Override
    public boolean checkProcessing() {
        return !pause;
    }

    public void pause(boolean pause) {
        this.pause = pause;
    }

}
