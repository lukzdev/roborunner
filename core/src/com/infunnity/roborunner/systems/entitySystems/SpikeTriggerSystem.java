package com.infunnity.roborunner.systems.entitySystems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.SpikeTriggerComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-20.
 */
public class SpikeTriggerSystem extends IteratingSystem {

    private ComponentMapper<PhysicsComponent> pMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<SpikeTriggerComponent> spikeMap = ComponentMapper.getFor(SpikeTriggerComponent.class);
    private TransformComponent tComp;
    private SpikeTriggerComponent spikeComp;
    private PhysicsComponent pComp;

    public SpikeTriggerSystem() {
        super(Family.getFor(TransformComponent.class, SpikeTriggerComponent.class));
    }

    private Vector2 tempPos = new Vector2(0,0);
    @Override
    public void processEntity(Entity entity, float deltaTime) {
        spikeComp = spikeMap.get(entity);
        pComp = pMap.get(entity);

        if(spikeComp.activated && spikeComp.activationState < 1) {
            spikeComp.activationState += deltaTime * 10;
            if(spikeComp.activationState > 1) {
                spikeComp.activationState = 1;
            }
//            tempPos.set(spikeComp.initPos);
            tempPos.set(pComp.body.getPosition().scl(Box2DWorld.BOX_TO_WORLD));
            tempPos.lerp(spikeComp.activePos, 0.3f * deltaTime * 60);
            tempPos.scl(Box2DWorld.WORLD_TO_BOX);

            pComp.body.setTransform(tempPos, pComp.body.getAngle());
        }

    }
}
