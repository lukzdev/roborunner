package com.infunnity.roborunner.systems.entitySystems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.infunnity.roborunner.components.entityComponents.SawComponent;
import com.infunnity.roborunner.components.TransformComponent;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class SawSystem extends IteratingSystem {

    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<SawComponent> sawMap = ComponentMapper.getFor(SawComponent.class);
    private TransformComponent tComp;

    public SawSystem() {
        super(Family.getFor(TransformComponent.class, SawComponent.class));
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        tComp = tMap.get(entity);
        tComp.rotation += deltaTime * sawMap.get(entity).spriteRotationSpeed * 360;
        tComp.rotation = tComp.rotation % 360;
    }
}
