package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.TextureRegionComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.components.WaypointComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class WaypointSystem extends IteratingSystem {

    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<WaypointComponent> wpMap = ComponentMapper.getFor(WaypointComponent.class);
    private ComponentMapper<PhysicsComponent> pMap = ComponentMapper.getFor(PhysicsComponent.class);

    private TransformComponent tComp;
    private WaypointComponent wComp;
    private PhysicsComponent pComp;

    public WaypointSystem() {
        super(Family.getFor(TransformComponent.class, WaypointComponent.class));
    }

    private Vector2 newPos = new Vector2();
    @Override
    public void processEntity(Entity entity, float deltaTime) {
        tComp = tMap.get(entity);
        wComp = wpMap.get(entity);
        pComp = pMap.get(entity);

        wComp.pointTime += deltaTime;

        // time exceeded, move to next point
        if(wComp.pointTime > wComp.waypointTimes.get(wComp.currentPoint)) {
            wComp.currentPoint++;
            wComp.pointTime = 0;

            // check if last point reached, restart
            if(wComp.currentPoint >= wComp.waypoints.size - 1) {
                wComp.currentPoint = 0;
            }
        }

        newPos.set(wComp.waypoints.get(wComp.currentPoint));
        newPos.lerp(wComp.waypoints.get(wComp.currentPoint + 1), wComp.pointTime / wComp.waypointTimes.get(wComp.currentPoint));

        // set new pos
        if(pComp != null) {
            pComp.body.setTransform(newPos.x * Box2DWorld.WORLD_TO_BOX, newPos.y * Box2DWorld.WORLD_TO_BOX, pComp.body.getAngle());
        }
        tComp.pos.set(newPos.x, newPos.y, tComp.pos.z);

    }
}
