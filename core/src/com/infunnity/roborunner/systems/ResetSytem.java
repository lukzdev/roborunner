package com.infunnity.roborunner.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.ResetComponent;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;

/**
 * Created by Lukasz on 2014-09-22.
 */
public class ResetSytem extends IteratingSystem {

    private ComponentMapper<ResetComponent> resetMapper = ComponentMapper.getFor(ResetComponent.class);

    public ResetSytem() {
//        super(Family.getFor(PlayerComponent.class, PhysicsComponent.class));
        super(Family.getFor(ResetComponent.class));
        this.setProcessing(false);
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        resetMapper.get(entity).resetInterface.reset();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        this.setProcessing(false);
    }
}
