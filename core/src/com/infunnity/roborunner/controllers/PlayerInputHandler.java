package com.infunnity.roborunner.controllers;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.models.entities.Player;
import com.infunnity.roborunner.systems.entitySystems.PlayerSystem;

/**
 * Created by Lukasz on 2014-08-05.
 */
public class PlayerInputHandler implements InputProcessor {

    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);

    private Player player;
    private PlayerComponent playerComponent;

    private Vector2 speedSliderPos = new Vector2();
    private float speedSliderTouchID = -1;
    private float jumpTouchID = -1;

    private float sliderMoveCm = 0;

    public PlayerInputHandler(Player player) {
        this.player = player;

        playerComponent = playerMap.get(player);
    }

    @Override
    public boolean keyDown(int keycode) {

        switch(keycode) {
            case Input.Keys.SPACE:
                playerComponent.jumpButton = true;
                break;
            case Input.Keys.RIGHT:
                playerComponent.rightButton = true;
                break;
            case Input.Keys.LEFT:
                playerComponent.leftButton = true;
                break;
            case Input.Keys.SHIFT_LEFT:
                playerComponent.boostButton = true;
                break;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch(keycode) {
            case Input.Keys.SPACE:
                playerComponent.jumpButton = false;
                playerComponent.jumpConsumed = false;
                break;
            case Input.Keys.RIGHT:
                playerComponent.rightButton = false;
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
                break;
            case Input.Keys.LEFT:
                playerComponent.leftButton = false;
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
                break;
            case Input.Keys.SHIFT_LEFT:
                playerComponent.boostButton = false;
                break;
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if(Gdx.graphics.getWidth() / 2 > screenX) {
            // Speed control
            if(speedSliderTouchID != -1)
                return false;

            speedSliderTouchID = pointer;
            speedSliderPos.x = screenX;
            speedSliderPos.y = screenY - Gdx.graphics.getPpcY() * 1;

        } else {
            // Jump
            jumpTouchID = pointer;
            playerComponent.jumpButton = true;
        }

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        // Check if jump released
        if(jumpTouchID == pointer) {
            jumpTouchID = -1;
            playerComponent.jumpButton = false;
            playerComponent.jumpConsumed = false;
            return true;
        }

        // Check if speed released
        if(speedSliderTouchID == pointer) {
            speedSliderTouchID = -1;
            playerComponent.rightButton = false;
            playerComponent.leftButton = false;
            playerComponent.boostButton = false;
            playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
            speedSliderPos.x = 0;
            speedSliderPos.y = 0;
            return true;
        }

        return false;
    }


    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if(speedSliderTouchID == pointer) {

            float valueChange = screenX - speedSliderPos.x;
            sliderMoveCm = valueChange / Gdx.graphics.getPpcX();

            // Check if direction changed - should reset wall time countdown
            if((playerComponent.rightButton && sliderMoveCm < 0) ||
                    (playerComponent.leftButton && sliderMoveCm > 0)) {
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
            }

            playerComponent.rightButton = false;
            playerComponent.leftButton = false;

            if(Math.abs(sliderMoveCm) >= 0.301f) {
                playerComponent.boostButton = true;
            } else {
                playerComponent.boostButton = false;
            }

            if(sliderMoveCm > 0.101) {
                // Right
                playerComponent.rightButton = true;
            } else if (sliderMoveCm < -0.101) {
                // Left
                playerComponent.leftButton = true;
            }

            // Move slider
            speedSliderPos.y = screenY - Gdx.graphics.getPpcY() * 1;
            if(sliderMoveCm > 0.4f) {
                speedSliderPos.x += (sliderMoveCm - 0.4f) * Gdx.graphics.getPpcX();
                sliderMoveCm = 0.4f;
            } else if (sliderMoveCm < -0.4f) {
                speedSliderPos.x += (sliderMoveCm + 0.4f) * Gdx.graphics.getPpcX();
                sliderMoveCm = -0.4f;
            }

            return true;
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public Vector2 getSpeedSliderPos() {
        return speedSliderPos;
    }

    public float getSpeedSliderTouchID() {
        return speedSliderTouchID;
    }

    public float getSliderMoveCm() {
        return sliderMoveCm;
    }
}
