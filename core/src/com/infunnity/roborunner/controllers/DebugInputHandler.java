package com.infunnity.roborunner.controllers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.models.GameWorld;
import com.infunnity.roborunner.screens.GameScreen;

/**
 * Created by Lukasz on 2014-08-05.
 */
public class DebugInputHandler implements InputProcessor {

    private RoboRunner game;
    private GameWorld world;

    public DebugInputHandler(RoboRunner game, GameWorld world) {
        this.game = game;
        this.world = world;
    }

    @Override
    public boolean keyDown(int keycode) {
        switch(keycode) {
            case Input.Keys.R:
                game.setScreen(new GameScreen(game));
                return true;
            case Input.Keys.D:
                RoboRunner.DEBUG = !RoboRunner.DEBUG;
                world.switchBox2DDebug();
                return true;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
