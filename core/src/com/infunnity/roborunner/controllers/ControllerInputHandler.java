package com.infunnity.roborunner.controllers;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.models.entities.Player;
import com.infunnity.roborunner.systems.entitySystems.PlayerSystem;
import com.infunnity.roborunner.utils.Xbox360Pad;

/**
 * Created by Lukasz on 2014-09-15.
 */
public class ControllerInputHandler implements ControllerListener {

    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);

    private Player player;
    private PlayerComponent playerComponent;

    public ControllerInputHandler(Player player) {
        this.player = player;

        playerComponent = playerMap.get(player);
    }

    @Override
    public void connected(Controller controller) {
        Gdx.app.log("CONTROLLER", "connected" + controller.getName());
    }

    @Override
    public void disconnected(Controller controller) {
        Gdx.app.log("CONTROLLER", "disconnected" + controller.getName());
    }

    @Override
    public boolean buttonDown(Controller controller, int buttonCode) {
        switch(buttonCode) {
            case Xbox360Pad.BUTTON_B:
                playerComponent.jumpButton = true;
            break;
            case Xbox360Pad.BUTTON_LB:
            case Xbox360Pad.BUTTON_RB:
            case Xbox360Pad.BUTTON_BACK:
                playerComponent.boostButton = true;
                break;
        }

        return true;
    }

    @Override
    public boolean buttonUp(Controller controller, int buttonCode) {
        switch(buttonCode) {
            case Xbox360Pad.BUTTON_B:
                playerComponent.jumpButton = false;
                playerComponent.jumpConsumed = false;
                break;
            case Xbox360Pad.BUTTON_LB:
            case Xbox360Pad.BUTTON_RB:
            case Xbox360Pad.BUTTON_BACK:
                playerComponent.boostButton = false;
                break;
        }

        return true;
    }


    float lastValue = 0;
    @Override
    public boolean axisMoved(Controller controller, int axisCode, float value) {
        if(axisCode == 1 || axisCode == 2) {
            lastValue = value;
            if((lastValue > 0 && value < 0) ||
                    (lastValue < 0 && value > 0)) {
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
            }

            if(value > 0) {
                playerComponent.rightButton = true;
                playerComponent.leftButton = false;
            } else {
                playerComponent.leftButton = true;
                playerComponent.rightButton = false;
            }

            if(Math.round(value) == 0) {
                playerComponent.rightButton = false;
                playerComponent.leftButton = false;
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
            }

            return true;
        }
        return false;
    }

    @Override
    public boolean povMoved(Controller controller, int povCode, PovDirection value) {
        switch(value) {
            case east:
            case southEast:
            case northEast:
                playerComponent.rightButton = true;
                playerComponent.leftButton = false;
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
            break;

            case west:
            case southWest:
            case northWest:
                playerComponent.leftButton = true;
                playerComponent.rightButton = false;
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
                break;

            case center:
                playerComponent.leftButton = false;
                playerComponent.rightButton = false;
                playerComponent.wallStickCountdown = PlayerSystem.wallStickTime;
            break;
        }

        return true;
    }

    @Override
    public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
        System.out.println(sliderCode+ " X " + value);
        return false;
    }

    @Override
    public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
        System.out.println(sliderCode+ " Y " + value);
        return false;
    }

    @Override
    public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
        return false;
    }
}
