package com.infunnity.roborunner.utils;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;

/**
 * Created by Lukasz on 2014-08-05.
 */
public class DebugBackgroundRenderer {

    private final static int GRADUALITY = 50;

    private ShapeRenderer sr;
    private Camera cam;

    public DebugBackgroundRenderer(Camera cam) {
        this.cam = cam;
        this.sr = new ShapeRenderer();

    }

    public void render() {
        this.sr.setProjectionMatrix(cam.combined);

        // Vertical
        int verticalLineX = Math.round((cam.position.x - cam.viewportWidth / 2) / GRADUALITY) * GRADUALITY;
        int verticalLineY = (int) (cam.position.y - cam.viewportHeight / 2);
        int verticalCount = MathUtils.ceilPositive(cam.viewportWidth / GRADUALITY);

        // Horizontal
        int horizontalLineY = Math.round((cam.position.y - cam.viewportHeight / 2) / GRADUALITY) * GRADUALITY;
        int horizontalLineX = (int) (cam.position.x - cam.viewportWidth / 2);
        int horizontalCount = MathUtils.ceilPositive(cam.viewportHeight / GRADUALITY);

        sr.setColor(1, 1, 1, 0.1f);
        sr.begin(ShapeRenderer.ShapeType.Line);

        // Vertical render
        for(int i = 0; i < verticalCount; i++) {
            sr.line(verticalLineX, verticalLineY, verticalLineX, verticalLineY + cam.viewportHeight);
            verticalLineX += GRADUALITY;
        }

        // Horizontal render
        for(int i = 0; i < horizontalCount; i++) {
            sr.line(horizontalLineX, horizontalLineY, horizontalLineX + cam.viewportWidth, horizontalLineY);
            horizontalLineY += GRADUALITY;
        }

        sr.end();
    }
}
