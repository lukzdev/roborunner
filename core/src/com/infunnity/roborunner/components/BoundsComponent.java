package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class BoundsComponent extends Component {
    public final Rectangle bounds = new Rectangle();
}
