package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.infunnity.roborunner.RoboRunner;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class CameraComponent extends Component {
    // Camera following target entity
    public Entity target;
    public OrthographicCamera camera;

    public float shakeTime = 0;
    public float shakeMagnitude = 1;

    public Rectangle bounding = new Rectangle();

    // bounding moved to right position and scaled
    public Rectangle actualBounding = new Rectangle();

    public CameraComponent(Entity target) {
        camera = new OrthographicCamera(RoboRunner.TARGET_WIDTH, RoboRunner.TARGET_HEIGHT);
        camera.zoom = 1;
        camera.position.set(camera.zoom * RoboRunner.TARGET_WIDTH / 2, camera.zoom * RoboRunner.TARGET_HEIGHT / 2, 0);

        this.target = target;
    }


}
