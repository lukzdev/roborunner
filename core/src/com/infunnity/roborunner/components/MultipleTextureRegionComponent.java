package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class MultipleTextureRegionComponent extends TextureRegionComponent {

//    public TextureRegion textureRegion;
//    public boolean isVisible = true;
//    public int layer = 0;
//
//    public MultipleTextureRegionComponent() {};
//    public MultipleTextureRegionComponent(TextureRegion textureRegion) {
//        this.textureRegion = textureRegion;
//    }

    public boolean xDirection = true;
    public int repeat = 1;

}
