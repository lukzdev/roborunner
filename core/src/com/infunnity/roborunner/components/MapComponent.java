package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.infunnity.roborunner.models.entities.Player;

/**
 * Created by Lukasz on 2014-09-19.
 */
public class MapComponent extends Component {

    public Player player;

    public Rectangle mapBounds = new Rectangle();

    public static final float MAX_DEAD_TIME = 2f;
    public float deadTime = 0f;

    public TiledMapTileLayer backgroundTiles;
    public MapLayer backgroundObjects;
    public TiledMapTileLayer groundTiles;
    public MapLayer groundObjects;
    public TiledMapTileLayer foregroundTiles;
    public MapLayer foregroundObjects;

    // Special Layers
    public TiledMapTileLayer crackTiles;

    public int tileWidth = 64;

    public Color backgroundTilesColor = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);
    public Color backgroundObjectsColor = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);
    public Color groundTilesColor = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);
    public Color groundObjectsColor = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);
    public Color foregroundTilesColor = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);
    public Color foregroundObjectsColor = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);

    public float zoom = 1f;

}
