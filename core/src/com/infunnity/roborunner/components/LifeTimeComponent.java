package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class LifeTimeComponent extends Component {

    public float lifeTime = 0;
}
