package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.infunnity.roborunner.RoboRunner;

/**
 * Created by Lukasz on 2014-09-15.
 */
public class GuiComponent extends Component {

    public GuiComponent(Stage stage) {
        this.stage = stage;
        movementSlider = new Slider(-40, 40, 20, false, RoboRunner.assets.get(RoboRunner.assets.Skin, Skin.class));
        movementSlider.setVisible(false);
        stage.addActor(movementSlider);
    }

    public Stage stage;
    public Slider movementSlider;
}
