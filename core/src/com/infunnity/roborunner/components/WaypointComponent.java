package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class WaypointComponent extends Component {
    public Array<Vector2> waypoints;// = new Array<Vector2>();
    public FloatArray waypointTimes;// = new FloatArray();

    public int currentPoint = 0;
    public float pointTime = 0;
}
