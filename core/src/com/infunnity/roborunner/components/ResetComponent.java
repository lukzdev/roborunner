package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class ResetComponent extends Component {

    public static interface ResetInterface {
        public void reset();
    }

    public ResetInterface resetInterface;

    public ResetComponent(ResetInterface resetInterface) {
        this.resetInterface = resetInterface;
    }

//    public abstract void reset();

}
