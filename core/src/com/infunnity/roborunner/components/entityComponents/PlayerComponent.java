package com.infunnity.roborunner.components.entityComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.utils.Array;
import com.infunnity.roborunner.systems.entitySystems.PlayerSystem;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class PlayerComponent extends Component {
    public static final int WIDTH = 35;
    public static final int HEIGHT = 62;

    public enum State {
        ON_GROUND, JUMPING, FALLING
    }

    public State state = State.ON_GROUND;

    public boolean isDead = false;

    public boolean jumpButton = false;
    public boolean rightButton = false;
    public boolean leftButton = false;
    public boolean boostButton = false;

    public boolean jumpConsumed = false; // Blocks multiple jumps on jump button hold

    public int onGround = 0;
    public int leftWall = 0;
    public int rightWall = 0;
    public boolean isGoingUp = false;

    public float wallStickCountdown = PlayerSystem.wallStickTime;

    public Vector2 startPos;

    // Not null when colliding with ground (player! not sensor!)
//    public Contact groundContactPoint;
    public Array<Contact> groundContactPoint = new Array<Contact>();

//    public static final float JUMP_VELOCITY = 11;
//    public static final float MOVE_VELOCITY = 20;
}
