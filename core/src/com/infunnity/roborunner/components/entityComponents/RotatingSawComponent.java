package com.infunnity.roborunner.components.entityComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class RotatingSawComponent extends Component {

    public float spriteRotationSpeed = 1;

    public float sawRotation = 0;
    public float rotationSpeed; // Speed in seconds
    public float rotationRadius;

    public Vector2 startPos = new Vector2(0,0);

}
