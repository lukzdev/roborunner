package com.infunnity.roborunner.components.entityComponents;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created by Lukasz on 2014-09-20.
 */
public class SpikeTriggerComponent extends Component {

    public Body triggerBody;

    public boolean activated = false;
    public float activationState = 0;

    public Vector2 initPos = new Vector2(0, 0);
    public Vector2 activePos = new Vector2(0, 0);

}
