package com.infunnity.roborunner.components.entityComponents;

import com.badlogic.ashley.core.Component;
import com.infunnity.roborunner.models.entities.Player;

/**
 * Created by Lukasz on 2014-09-09.
 */
public class OnWallSensorComponent extends Component {
    public int width;
    public int height;

    public boolean left;

    public Player player;
}
