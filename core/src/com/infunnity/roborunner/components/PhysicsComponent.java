package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class PhysicsComponent extends Component {

    public static interface PhysicsListener {
        public void beginContact(Entity e, Contact contact);
        public void endContact(Entity e, Contact contact);
        public void preSolve(Entity e, Contact contact);
        public void postSolve(Entity e, Contact contact, ContactImpulse impulse);
    }

    public Body body;
    public BodyDef.BodyType bodyType;
    public PhysicsListener physicsListener;

    public PhysicsComponent(Body body) {
        this(body, null);
    }

    public PhysicsComponent(Body body, PhysicsListener physicsListener) {
        this.body = body;
        this.bodyType = body.getType();
        this.physicsListener = physicsListener;
    }

}
