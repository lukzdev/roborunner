package com.infunnity.roborunner.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class TextureRegionComponent extends Component {

    public TextureRegion textureRegion;
    public boolean isVisible = true;
    public int layer = 0;

    public int offsetX = 0;
    public int offsetY = 0;

    public int originX = 0;
    public int originY = 0;
    public boolean originSet = false;


    public TextureRegionComponent() {};
    public TextureRegionComponent(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }
}
