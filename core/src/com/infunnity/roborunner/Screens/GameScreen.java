package com.infunnity.roborunner.screens;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Screen;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.models.GameWorld;
import com.infunnity.roborunner.systems.RenderingSystem;

/**
 * Created by Lukasz on 2014-08-03.
 */
public class GameScreen implements Screen {

    private RoboRunner game;

    private Engine entEngine;
    private GameWorld world;

    /*
	 * Things for Fixed Timestep - look into render for implementation and docs
	 */
    private float fixedTimestepAccumulator = 0f;
    private final float MAX_ACCUMULATED_TIME = 1.0f;
    private final float TIMESTEP = 1/60f;
    private int steps = 0;

    public GameScreen(RoboRunner game) {
        this.game = game;

        this.entEngine = new Engine();
        this.world = new GameWorld(game, entEngine);
    }

    @Override
    public void render(float delta) {
        /*
		 * Implementation of fixed timestep
		 * docs:
		 * - http://gafferongames.com/game-physics/fix-your-timestep/
		 * - http://temporal.pr0.pl/devblog/download/arts/fixed_step/fixed_step.pdf
		 */
        // TODO: possible improvement: http://siondream.com/blog/games/fixing-your-timestep-in-libgdx-and-box2d/
        fixedTimestepAccumulator += delta;
        if(fixedTimestepAccumulator > MAX_ACCUMULATED_TIME)
            fixedTimestepAccumulator = MAX_ACCUMULATED_TIME;

        while (fixedTimestepAccumulator >= TIMESTEP) {

			/*
			 * Update physics
			 */
//            world.update(TIMESTEP);
//            entEngine.update(TIMESTEP);

            fixedTimestepAccumulator -= TIMESTEP;
        }

        world.update(delta);
        /*
         * Update physics
         */
        entEngine.update(delta);

//        entEngine.getSystem(RenderingSystem.class).update(delta);
    }

    @Override
    public void resize(int width, int height) {
        //render.resize(width, height);
    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        world.dispose();
    }
}
