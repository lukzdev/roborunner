package com.infunnity.roborunner;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.FPSLogger;
import com.infunnity.roborunner.models.Assets;
import com.infunnity.roborunner.screens.GameScreen;

public class RoboRunner extends Game {

    public static boolean DEBUG = false;

    public static Assets assets;

    public static final float TARGET_WIDTH = 1024;
    public static final float TARGET_HEIGHT = 576;

    FPSLogger log;

	@Override
	public void create () {
        log = new FPSLogger();
        assets = new Assets();

        this.setScreen(new GameScreen(this));
	}

	@Override
	public void render () {
        super.render();
        log.log();
	}
}
