package com.infunnity.roborunner.models;

import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.AtlasTmxMapLoader;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Lukasz on 2014-08-06.
 */
public class Assets {

    public AssetManager manager;

    // Textures
    public final String TestSet = "tilesets/testset.atlas";
    public final String ObstacleAtlas = "tilesets/obstacles.pack";
    public final String PlayerAtlas = "tilesets/player.pack";
    public final String TempBG = "bgs/tempbg.png";

    // Maps
    public final String TestMap = "maps/testmap.tmx";

    // Map Atlases - loaded as dependencies
    public final String SpaceFactoryAtlas = "maps/1_spacefactoryset.pack";

    public final String Map1 = "maps/map1.tmx";
    public final String Map2 = "maps/map2.tmx";
    public final String Map3 = "maps/map3.tmx";
    public final String Map4 = "maps/map4.tmx";

    // Skin
    public final String Skin = "uiskin.json";

    // Music
    public final String MusicEdgeOfSolance = "music/Deatron - Edge of Solace.ogg";

    // Sounds
    public final String LandSound = "sounds/jumpland.wav";

    // Particles
    public String runParticle = "particles/running_dirt.p";
    public String jumpParticle = "particles/jump.p";
    public String sparkParticle = "particles/spark.p";

    public Assets() {
        manager = new AssetManager();
        enqueueAssets();
        manager.finishLoading();
    }

    public void enqueueAssets() {
//        manager.load(TempBG, Texture.class);

//        manager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
        manager.load(ObstacleAtlas, TextureAtlas.class);
        manager.load(PlayerAtlas, TextureAtlas.class);

        manager.load(Skin, Skin.class);

        ParticleEffectLoader.ParticleEffectParameter particleParameters = new ParticleEffectLoader.ParticleEffectParameter();
        particleParameters.atlasFile = PlayerAtlas;
        manager.load(runParticle, ParticleEffect.class, particleParameters);
        manager.load(jumpParticle, ParticleEffect.class, particleParameters);
        manager.load(sparkParticle, ParticleEffect.class, particleParameters);

        AtlasTmxMapLoader.AtlasTiledMapLoaderParameters params = new AtlasTmxMapLoader.AtlasTiledMapLoaderParameters();
        manager.setLoader(TiledMap.class, new AtlasTmxMapLoader(new InternalFileHandleResolver()));
//        manager.load(Map1, TiledMap.class);
//        manager.load(Map2, TiledMap.class);
        manager.load(Map3, TiledMap.class);
        manager.load(Map4, TiledMap.class);

        manager.load(MusicEdgeOfSolance, Music.class);
        manager.load(LandSound, Sound.class);

    }

    public boolean update() {
        return manager.update();
    }

    public boolean isLoaded(String fileName) {
        return manager.isLoaded(fileName);
    }

    public float getProgress() {
        return manager.getProgress();
    }

    public <T> T get (String fileName, Class<T> type) {
        return manager.get(fileName, type);
    }

    public void dispose() {
        manager.dispose();
    }
}
