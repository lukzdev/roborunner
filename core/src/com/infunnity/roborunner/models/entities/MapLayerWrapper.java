package com.infunnity.roborunner.models.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

/**
 * Created by Lukasz on 2014-08-06.
 */
public class MapLayerWrapper extends TiledMapTileLayer {

    private Color color;

    /**
     * Creates TiledMap layer
     *
     * @param width      layer width in tiles
     * @param height     layer height in tiles
     * @param tileWidth  tile width in pixels
     * @param tileHeight tile height in pixels
     */
    public MapLayerWrapper(int width, int height, int tileWidth, int tileHeight, Color col) {
        super(width, height, tileWidth, tileHeight);
        this.color = new Color(col.r, col.g, col.b, col.a);
    }

    public MapLayerWrapper(int width, int height, int tileWidth, int tileHeight) {
        super(width, height, tileWidth, tileHeight);
        this.color = new Color(Color.WHITE.r, Color.WHITE.g, Color.WHITE.b, Color.WHITE.a);
    }

//
//    public MapLayerWrapper(Color col) {
//        this(col.r, col.g, col.b, col.a);
//    }
//
//    public MapLayerWrapper(float r, float g, float b, float a) {
//        color = new Color(r, g, b, a);
//    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
