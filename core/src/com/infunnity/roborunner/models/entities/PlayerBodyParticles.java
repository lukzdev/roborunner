package com.infunnity.roborunner.models.entities;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.models.Box2DWorld;
import com.infunnity.roborunner.systems.ParticleSystem;
import com.infunnity.roborunner.utils.BodyEditorLoader;

/**
 * Created by Lukasz on 2014-09-25.
 */
public class PlayerBodyParticles extends Entity {

    private BodyEditorLoader loader;

    private TextureRegion bodyTex;
    private TextureRegion headTex;
    private TextureRegion leftLegTex;
    private TextureRegion rightLegTex;
    private TextureRegion leftArmTex;
    private TextureRegion rightArmTex;

    private Entity bodyEntity;
    private Entity headEntity;
    private Entity leftArmEntity;
    private Entity rightArmEntity;
    private Entity leftLegEntity;
    private Entity rightLegEntity;

    public boolean toActivate = false;
    public boolean toDeactivate = false;

    private Engine engine;
    private ParticleSystem particleSystem;

    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<TextureRegionComponent> texMap = ComponentMapper.getFor(TextureRegionComponent.class);

    public PlayerBodyParticles(float x, float y, Box2DWorld world, final Engine engine) {

        TextureAtlas atlas = RoboRunner.assets.get(RoboRunner.assets.PlayerAtlas, TextureAtlas.class);

        bodyTex = atlas.findRegion("armor1");
        headTex = atlas.findRegion("armor6");
        leftLegTex = atlas.findRegion("armor2");
        rightLegTex = atlas.findRegion("armor3");
        leftArmTex = atlas.findRegion("armor4");
        rightArmTex = atlas.findRegion("armor5");

        this.engine = engine;

        PhysicsComponent.PhysicsListener physicsListener = new PhysicsComponent.PhysicsListener() {
            @Override
            public void beginContact(Entity e, Contact contact) {

            }

            @Override
            public void endContact(Entity e, Contact contact) {

            }

            @Override
            public void preSolve(Entity e, Contact contact) {

            }

            @Override
            public void postSolve(Entity e, Contact contact, ContactImpulse impulse) {
                Entity self = (Entity)contact.getFixtureA().getBody().getUserData() == e ? (Entity)contact.getFixtureB().getBody().getUserData() : (Entity)contact.getFixtureA().getBody().getUserData();

                if(particleSystem == null) {
                    particleSystem = engine.getSystem(ParticleSystem.class);
                }

                if(!(e instanceof Saw || e instanceof RotatingSaw || e instanceof MovingSaw || e instanceof TriggerSpikes )) {
                    return;
                }

                // Dont spawn if not seen
                CameraComponent camComp = engine.getEntitiesFor(Family.getFor(CameraComponent.class)).first().getComponent(CameraComponent.class);
                if(!camComp.actualBounding.contains(tMap.get(self).pos.x, tMap.get(self).pos.y))
                    return;

                // Don't spawn particles if low framerate
                if(Gdx.graphics.getFramesPerSecond() < 50) {
                    return;
                }

                // Sparks
                ParticleEffectPool.PooledEffect effect = particleSystem.obtainEffect(ParticleSystem.SPARK_PARTICLE);
                effect.setPosition(contact.getWorldManifold().getPoints()[0].x * Box2DWorld.BOX_TO_WORLD, contact.getWorldManifold().getPoints()[0].y * Box2DWorld.BOX_TO_WORLD);

                // HARDCODED 20!
                effect.getEmitters().get(0).setMaxParticleCount((int) (20*MathUtils.clamp(impulse.getNormalImpulses()[0], 0.2f, 1)));
                particleSystem.addEffect(effect);
            }
        };

        loader = new BodyEditorLoader(Gdx.files.internal("bodies"));

        bodyEntity = makeNewParticleEntity(x, y, 0, bodyTex, "armor1.png", world, physicsListener);
        headEntity = makeNewParticleEntity(x, y + 21, 0, headTex, "armor6.png", world, physicsListener);

        leftArmEntity = makeNewParticleEntity(x + 22, y - 3, 0, leftArmTex, "armor4.png", world, physicsListener);
        rightArmEntity = makeNewParticleEntity(x - 8, y - 3, 0, rightArmTex, "armor5.png", world, physicsListener);

        leftLegEntity = makeNewParticleEntity(x - 10, y - 20, 0, leftLegTex, "armor2.png", world, physicsListener);
        rightLegEntity = makeNewParticleEntity(x + 12, y - 20, 0, rightLegTex, "armor3.png", world, physicsListener);

        engine.addEntity(bodyEntity);
        engine.addEntity(headEntity);
        engine.addEntity(leftArmEntity);
        engine.addEntity(rightArmEntity);
        engine.addEntity(leftLegEntity);
        engine.addEntity(rightLegEntity);
    }

    public Entity makeNewParticleEntity(float x, float y, float rotation, TextureRegion texRegion, String bodyName, Box2DWorld world, PhysicsComponent.PhysicsListener listener) {
        // New Entity
        Entity particleEntity = new Entity();

        // New body
        FixtureDef fdef = new FixtureDef();
        fdef.density = 0.5f;
        fdef.friction = 0.3f;
        fdef.restitution = 0.3f;
        fdef.filter.categoryBits = Box2DWorld.CATEGORY.PLAYER_PARTICLES;
        fdef.filter.maskBits = Box2DWorld.PARTICLE_MASK;

        BodyDef bd = new BodyDef();
        bd.position.set(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX);
        bd.allowSleep = true;
        bd.type = BodyDef.BodyType.DynamicBody;
        bd.angle = rotation * MathUtils.degRad;
        bd.active = false;

        Body body = world.getWorld().createBody(bd);
        body.setUserData(particleEntity);
        loader.attachFixture(body, bodyName, fdef, texRegion.getRegionWidth()*Box2DWorld.WORLD_TO_BOX);

        // New Entity components
        TextureRegionComponent tr = new TextureRegionComponent(texRegion);
        tr.originSet = true;
        tr.isVisible = false;

        PhysicsComponent pc = new PhysicsComponent(body, listener);
        particleEntity.add(new TransformComponent());
        particleEntity.add(tr);
        particleEntity.add(pc);

        return particleEntity;
    }

    private Vector2 velocity = new Vector2(0,0);
    private Vector2 position = new Vector2(0,0);
    public void reposition(float x, float y, float velX, float velY) {
        x -= 12;
        y -= 10;

        velocity.set(velX * 0.7f, velY * 0.7f);
        position.set(x, y);
    }

    public void deactivate() {
        toDeactivate = false;
        physMap.get(bodyEntity).body.setActive(false);
        physMap.get(headEntity).body.setActive(false);
        physMap.get(leftArmEntity).body.setActive(false);
        physMap.get(rightArmEntity).body.setActive(false);
        physMap.get(leftLegEntity).body.setActive(false);
        physMap.get(rightLegEntity).body.setActive(false);

        texMap.get(bodyEntity).isVisible = false;
        texMap.get(headEntity).isVisible = false;
        texMap.get(leftArmEntity).isVisible = false;
        texMap.get(rightArmEntity).isVisible = false;
        texMap.get(leftLegEntity).isVisible = false;
        texMap.get(rightLegEntity).isVisible = false;
    }

    public void activate() {
        toActivate = false;

        float x = position.x;
        float y = position.y;
        float velX = velocity.x;
        float velY = velocity.y;

        physMap.get(bodyEntity).body.setActive(true);
        physMap.get(headEntity).body.setActive(true);
        physMap.get(leftArmEntity).body.setActive(true);
        physMap.get(rightArmEntity).body.setActive(true);
        physMap.get(leftLegEntity).body.setActive(true);
        physMap.get(rightLegEntity).body.setActive(true);

        texMap.get(bodyEntity).isVisible = true;
        texMap.get(headEntity).isVisible = true;
        texMap.get(leftArmEntity).isVisible = true;
        texMap.get(rightArmEntity).isVisible = true;
        texMap.get(leftLegEntity).isVisible = true;
        texMap.get(rightLegEntity).isVisible = true;

        /* to make not coliding */
        physMap.get(bodyEntity).body.setTransform(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 0);
        physMap.get(headEntity).body.setTransform(x * Box2DWorld.WORLD_TO_BOX, ((y + 21) + /**/5/**/) * Box2DWorld.WORLD_TO_BOX, 0);
        physMap.get(leftArmEntity).body.setTransform(((x + 22) + /**/5/**/) * Box2DWorld.WORLD_TO_BOX, (y - 3) * Box2DWorld.WORLD_TO_BOX, 0);
        physMap.get(rightArmEntity).body.setTransform(((x - 8) - /**/5/**/) * Box2DWorld.WORLD_TO_BOX, (y - 3) * Box2DWorld.WORLD_TO_BOX, 0);
        physMap.get(leftLegEntity).body.setTransform((x - 10) * Box2DWorld.WORLD_TO_BOX, ((y - 20) - /**/5/**/) * Box2DWorld.WORLD_TO_BOX, 0);
        physMap.get(rightLegEntity).body.setTransform((x + 12 + /**/5/**/) * Box2DWorld.WORLD_TO_BOX, ((y - 20) - /**/5/**/) * Box2DWorld.WORLD_TO_BOX, 0);

        physMap.get(bodyEntity).body.setLinearVelocity(velX + MathUtils.random(-1, 1), velY + MathUtils.random(-1, 1));
        physMap.get(headEntity).body.setLinearVelocity(velX + MathUtils.random(-1, 1), velY + MathUtils.random(-1, 1));
        physMap.get(leftArmEntity).body.setLinearVelocity(velX + MathUtils.random(-1, 1), velY + MathUtils.random(-1, 1));
        physMap.get(rightArmEntity).body.setLinearVelocity(velX + MathUtils.random(-1, 1), velY + MathUtils.random(-1, 1));
        physMap.get(leftLegEntity).body.setLinearVelocity(velX + MathUtils.random(-1, 1), velY + MathUtils.random(-1, 1));
        physMap.get(rightLegEntity).body.setLinearVelocity(velX + MathUtils.random(-1, 1), velY + MathUtils.random(-1, 1));
    }





}
