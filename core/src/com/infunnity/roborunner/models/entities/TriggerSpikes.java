package com.infunnity.roborunner.models.entities;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.components.entityComponents.SpikeTriggerComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class TriggerSpikes extends Entity {

    private ComponentMapper<SpikeTriggerComponent> spikeMap = ComponentMapper.getFor(SpikeTriggerComponent.class);

    private ComponentMapper<TransformComponent> transMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);

    public static enum SpikesDirection {
        UP, DOWN, LEFT, RIGHT
    }

    private final float SPIKE_BOUND_OFFSET = 7 * 2;

    public TriggerSpikes(float x, float y, float width, SpikesDirection direction,  Box2DWorld box2DWorld) {
        super();

        // Create body
        Body body = box2DWorld.getBodyBuilder()
                //.mass(1f)
                .fixture(box2DWorld.getFixtureDefBuilder()
                        .boxShape(width / 2 - SPIKE_BOUND_OFFSET, 60)
                        .density(1f)
                        .friction(1f)
                        .restitution(0f)
                        .maskBits(Box2DWorld.OBSTACLE_MASK)
                        .categoryBits(Box2DWorld.CATEGORY.OBSTACLE)
                        .build())
//                .angle(0.12f)
//                .position(x, y)
                .type(BodyDef.BodyType.KinematicBody)
                .userData(this)
                .build();


        TransformComponent position = new TransformComponent();
        position.pos.set(x, y, 0);

        float angle = 0;
        // Position body
        switch(direction) {
            case UP:
                x = x + width / 2;
                y = y + 128 / 2;
                body.setTransform(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 0);
                break;
            case DOWN:
                x = x + width / 2;
                y = y + 128 / 2;
                body.setTransform(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 180 * MathUtils.degRad);
                break;
            case LEFT:
                y = y + width / 2;
                x = x + 128 / 2;
                body.setTransform(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 90 * MathUtils.degRad);
                angle = 90;
                break;
            case RIGHT:
                y = y + width / 2;
                x = x + 128 / 2;
                body.setTransform(x * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 270 * MathUtils.degRad);
                angle = 90;
                break;
        }

        position.rotation = angle;

        // Create trigger sensor
        Body triggerBody = box2DWorld.getBodyBuilder()
                //.mass(1f)
                .fixture(box2DWorld.getFixtureDefBuilder()
                        .boxShape(width / 2 - SPIKE_BOUND_OFFSET, 60)
                        .density(1f)
                        .friction(1f)
                        .restitution(0f)
                        .maskBits(Box2DWorld.OBSTACLE_MASK)
                        .categoryBits(Box2DWorld.CATEGORY.OBSTACLE)
                        .sensor()
                        .build())
//                .angle(0.12f)
//                .position(x, y)
                .type(BodyDef.BodyType.StaticBody)
                .userData(this)
                .build();

        float activePosX = 0;
        float activePosY = 0;

        // Position trigger
        switch(direction) {
            case UP:
                triggerBody.setTransform(x * Box2DWorld.WORLD_TO_BOX, (y + 120) * Box2DWorld.WORLD_TO_BOX, 0);
                activePosX = x;
                activePosY = (y + 64);
                break;
            case DOWN:
                triggerBody.setTransform(x * Box2DWorld.WORLD_TO_BOX, (y - 120) * Box2DWorld.WORLD_TO_BOX, 0);
                activePosX = x;
                activePosY = (y - 64);
                break;
            case LEFT:
                triggerBody.setTransform((x - 120) * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 90 * MathUtils.degRad);
                activePosX = (x - 64);
                activePosY = y;
                break;
            case RIGHT:
                triggerBody.setTransform((x + 120) * Box2DWorld.WORLD_TO_BOX, y * Box2DWorld.WORLD_TO_BOX, 90 * MathUtils.degRad);
                activePosX = (x + 64);
                activePosY = y;
                break;
        }


//        BoundsComponent bounds = new BoundsComponent();
        PhysicsComponent physics = new PhysicsComponent(body, new PhysicsComponent.PhysicsListener() {
            @Override
            public void endContact(Entity e, Contact contact) {

            }

            @Override
            public void preSolve(Entity e, Contact contact) {

            }

            @Override
            public void postSolve(Entity e, Contact contact, ContactImpulse impulse) {

            }

            @Override
            public void beginContact(Entity e, Contact contact) {
                if(!spikeMap.get(TriggerSpikes.this).activated) {
                    spikeMap.get(TriggerSpikes.this).activated = true;
                }
            }
        });
        physics.body = body;


        MultipleTextureRegionComponent texRegion = new MultipleTextureRegionComponent();
        texRegion.textureRegion = RoboRunner.assets.get(RoboRunner.assets.ObstacleAtlas, TextureAtlas.class).findRegion("spikes");
        texRegion.layer = 1;

        switch(direction) {
            case UP:
                texRegion.xDirection = true;
                break;
            case DOWN:
                texRegion.xDirection = true;
                break;
            case LEFT:
                texRegion.xDirection = false;
                break;
            case RIGHT:
                texRegion.xDirection = false;
                break;
        }


        texRegion.repeat = (int)(width / 64);

        SpikeTriggerComponent spikeComponent = new SpikeTriggerComponent();
        spikeComponent.initPos.set(body.getPosition().scl(Box2DWorld.BOX_TO_WORLD));
        spikeComponent.activePos.set(activePosX, activePosY);
        spikeComponent.activated = false;

        spikeComponent.triggerBody = triggerBody;

//        texRegion2.textureRegion
//        bounds.bounds.width = Math.round(radius*2);
//        bounds.bounds.height = Math.round(radius*2);

        ResetComponent resetComp = new ResetComponent(new ResetComponent.ResetInterface() {
            @Override
            public void reset() {
                spikeMap.get(TriggerSpikes.this).activated = false;
                spikeMap.get(TriggerSpikes.this).activationState = 0;

//                transMap.get(TriggerSpikes.this).pos.set(spikeMap.get(TriggerSpikes.this).initPos, 0);
                physMap.get(TriggerSpikes.this).body.setTransform(spikeMap.get(TriggerSpikes.this).initPos.x * Box2DWorld.WORLD_TO_BOX,
                        spikeMap.get(TriggerSpikes.this).initPos.y * Box2DWorld.WORLD_TO_BOX, physMap.get(TriggerSpikes.this).body.getAngle());
            }
        });

        this.add(resetComp);
        this.add(physics);
//        SawComponent sawComponent = new SawComponent();
//        sawComponent.spriteRotationSpeed = bladeRotationSpeed;
//
//        this.add(sawComponent);
        this.add(position);
//        this.add(bounds);
        this.add(texRegion);
        this.add(spikeComponent);

    }
}
