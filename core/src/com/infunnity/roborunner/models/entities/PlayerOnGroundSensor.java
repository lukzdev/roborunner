package com.infunnity.roborunner.models.entities;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.infunnity.roborunner.components.TransformComponent;
import com.infunnity.roborunner.components.entityComponents.OnGroundSensorComponent;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.models.Box2DWorld;
import com.infunnity.roborunner.models.Map;
import com.infunnity.roborunner.systems.entitySystems.PlayerSystem;

/**
 * Created by Lukasz on 2014-09-09.
 */

public class PlayerOnGroundSensor extends Entity {

    private ComponentMapper<TransformComponent> tMap = ComponentMapper.getFor(TransformComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<OnGroundSensorComponent> onGroundMap = ComponentMapper.getFor(OnGroundSensorComponent.class);

    private Engine engine;

    public PlayerOnGroundSensor(float x, float y, float width, float height, Player player, Box2DWorld box2dworld, final Engine engine) {
        super();

        Body body = box2dworld.getBodyBuilder()
                .fixture(box2dworld.getFixtureDefBuilder()
                        .boxShape(width, height)
                        .density(1f)
                        .friction(0f)
                        .restitution(0f)
                        .categoryBits(Box2DWorld.CATEGORY.SENSOR)
                        .maskBits(Box2DWorld.SENSOR_MASK)
                        .sensor()
                        .build())
                .position(x, y)
                .fixedRotation()
                .type(BodyDef.BodyType.DynamicBody)
                .userData(this)
                .build();

        this.engine = engine;

        PhysicsComponent physComp = new PhysicsComponent(body, new PhysicsComponent.PhysicsListener() {
            @Override
            public void beginContact(Entity e, Contact contact) {
                if(e instanceof Map) {
                    if(playerMap.get(onGroundMap.get(PlayerOnGroundSensor.this).player).onGround == 0) {
                        engine.getSystem(PlayerSystem.class).jumpParticles();
                    }

                    playerMap.get(onGroundMap.get(PlayerOnGroundSensor.this).player).onGround++;
                    return;
                }
            }

            @Override
            public void endContact(Entity e, Contact contact) {
                if(e instanceof Map) {
                    playerMap.get(onGroundMap.get(PlayerOnGroundSensor.this).player).onGround--;
                    return;
                }
            }

            @Override
            public void preSolve(Entity e, Contact contact) {
            }

            @Override
            public void postSolve(Entity e, Contact contact, ContactImpulse impulse) {

            }
        });

        OnGroundSensorComponent onGroundComp = new OnGroundSensorComponent();
        onGroundComp.player = player;
        onGroundComp.width = (int)width;
        onGroundComp.height = (int)height;

        this.add(onGroundComp);
        this.add(physComp);
    }

//        @Override
//        public void handleBeginContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {
//            if(psycho2 instanceof Ground || psycho2 instanceof  GhostPlayer) {
//                player.onGround++;
//                return;
//            }
//        }
//
//        @Override
//        public void handleEndContact(PhysicsObject psycho2, Vector2[] contactPoints, Map map) {
//            if(psycho2 instanceof Ground || psycho2 instanceof  GhostPlayer) {
//                player.onGround--;
//                return;
//            }
//        }
}
