package com.infunnity.roborunner.models.entities;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.infunnity.roborunner.components.entityComponents.OnWallSensorComponent;
import com.infunnity.roborunner.components.PhysicsComponent;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.models.Box2DWorld;
import com.infunnity.roborunner.models.Map;

/**
 * Created by Lukasz on 2014-09-09.
 */

public class PlayerOnWallSensor extends Entity {

    private ComponentMapper<PhysicsComponent> playerPhysMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<OnWallSensorComponent> onWallMap = ComponentMapper.getFor(OnWallSensorComponent.class);



    public PlayerOnWallSensor(float x, float y, float width, float height, Player player, Box2DWorld box2dworld, boolean left) {
        super();

        Body body = box2dworld.getBodyBuilder()
                .fixture(box2dworld.getFixtureDefBuilder()
                        .boxShape(width, height)
                        .density(1f)
                        .categoryBits(Box2DWorld.CATEGORY.SENSOR)
                        .maskBits(Box2DWorld.SENSOR_MASK)
                        .friction(0f)
                        .restitution(0f)
                        .sensor()
                        .build())
                .position(x, y)
                .fixedRotation()
                .type(BodyDef.BodyType.DynamicBody)
                .userData(this)
                .build();

        PhysicsComponent physComp = new PhysicsComponent(body, new PhysicsComponent.PhysicsListener() {
            @Override
            public void beginContact(Entity e, Contact contact) {
                if(e instanceof Map) {
                    playerPhysMap.get(onWallMap.get(PlayerOnWallSensor.this).player).body.setGravityScale(0.66f);
                    if(onWallMap.get(PlayerOnWallSensor.this).left) {
                        playerMap.get(onWallMap.get(PlayerOnWallSensor.this).player).leftWall++;
                    } else {
                        playerMap.get(onWallMap.get(PlayerOnWallSensor.this).player).rightWall++;
                    }
                    return;
                }
            }

            @Override
            public void endContact(Entity e, Contact contact) {
                if(e instanceof Map) {
                    playerPhysMap.get(onWallMap.get(PlayerOnWallSensor.this).player).body.setGravityScale(1f);
                    if(onWallMap.get(PlayerOnWallSensor.this).left) {
                        playerMap.get(onWallMap.get(PlayerOnWallSensor.this).player).leftWall--;
                    } else {
                        playerMap.get(onWallMap.get(PlayerOnWallSensor.this).player).rightWall--;
                    }
                    return;
                }
            }

            @Override
            public void preSolve(Entity e, Contact contact) {

            }

            @Override
            public void postSolve(Entity e, Contact contact, ContactImpulse impulse) {

            }
        });

        OnWallSensorComponent onWallComp = new OnWallSensorComponent();
        onWallComp.player = player;
        onWallComp.width = (int)width;
        onWallComp.height = (int)height;
        onWallComp.left = left;

        this.add(onWallComp);
        this.add(physComp);
    }
}
