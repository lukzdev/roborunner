package com.infunnity.roborunner.models.entities;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.components.entityComponents.SpikeTriggerComponent;
import com.infunnity.roborunner.models.Box2DWorld;
import com.infunnity.roborunner.models.Map;
import com.infunnity.roborunner.models.SoundManager;
import com.infunnity.roborunner.systems.MapSystem;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class Player extends Entity {

    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);
    private ComponentMapper<PhysicsComponent> playerPhysMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<SpikeTriggerComponent> triggMap = ComponentMapper.getFor(SpikeTriggerComponent.class);
//    private final static short PLAYER_MASK = Box2DWorld.CATEGORY. | Box2dWorld.CATEGORY.GROUND | Box2dWorld.CATEGORY.WHEEL_HUB;

    private SoundManager soundManager;

    public PlayerBodyParticles pbp;

    public Player(Vector2 pos, Box2DWorld box2DWorld, final Engine engine) {
        super();

        pbp = new PlayerBodyParticles(-100, -100, box2DWorld, engine);

        Body body = box2DWorld.getBodyBuilder()
                //.mass(1f)
                .fixture(box2DWorld.getFixtureDefBuilder()
                        .boxShape(PlayerComponent.WIDTH / 2, PlayerComponent.HEIGHT / 2)
                        .density(1f)
                        .friction(0f)
                        .restitution(0f)
                        .categoryBits(Box2DWorld.CATEGORY.PLAYER)
                        .build())
                .fixedRotation()
                .position(pos.x, pos.y)
                .mass(0.25f)
//                .angle(0.12f)
                .type(BodyDef.BodyType.DynamicBody)
                .userData(this)
                .build();

        TransformComponent position = new TransformComponent();
        BoundsComponent bounds = new BoundsComponent();
        PlayerComponent player = new PlayerComponent();
        player.startPos = pos;

        soundManager = SoundManager.getInstance();

        PhysicsComponent physics = new PhysicsComponent(body, new PhysicsComponent.PhysicsListener() {

            private Vector2 temp = new Vector2(0,0);
            private float scalar = 0;

            @Override
            public void beginContact(Entity e, Contact contact) {
                if(!(e instanceof Map)) {
                    // Don't kill for hitting enemy sensors!
                    // TODO: UGLYYYY!!!!
                    if(e instanceof TriggerSpikes) {
                        if(contact.getFixtureA().getBody() == triggMap.get(e).triggerBody ||
                                contact.getFixtureB().getBody() == triggMap.get(e).triggerBody)
                        return;
                    }

                    // Shake camera
                    CameraComponent camComp = engine.getEntitiesFor(Family.getFor(CameraComponent.class)).first().getComponent(CameraComponent.class);
                    camComp.shakeTime = 0.25f;

                    camComp.shakeMagnitude = 1.2f;
                    engine.getSystem(MapSystem.class).killPlayer();

                    return;
                }

                if(e instanceof Map) {
                    // Set contact point for crack checks in PlayerSystem
                    playerMap.get(Player.this).groundContactPoint.add(contact);
//                    playerMap.get(Player.this).groundContactPoint = contact;

                    // Calculate landing sound volume
                    temp.set(contact.getWorldManifold().getNormal().x, contact.getWorldManifold().getNormal().y);
                    temp.scl(playerPhysMap.get(Player.this).body.getLinearVelocity());
                    scalar = Math.abs(temp.x) + Math.abs(temp.y);
                    scalar = (scalar - 2) / 4;
                    soundManager.playLandSound(scalar);


                    // Some subtle shaking?
                    CameraComponent camComp = engine.getEntitiesFor(Family.getFor(CameraComponent.class)).first().getComponent(CameraComponent.class);
                    if(scalar > 1) {
//                        scalar -= 1; // 0 - 1.5

                        float shakeTime = 0.1f * (scalar - 1); // -1 to get it to 0-1.5
                        if(shakeTime > 0.05f) {
                            shakeTime = 0.05f;
                        }

                        camComp.shakeTime = shakeTime;
                        camComp.shakeMagnitude = 3.5f * shakeTime;

                        if(camComp.shakeMagnitude < 0.6f)
                            camComp.shakeMagnitude = 0.6f;
                    }

                    //Crack that!
                    Map map = (Map)e;
                    Vector2[] contactPoints = contact.getWorldManifold().getPoints();
                    if(contactPoints.length == 2 && scalar > 1) {
                        map.findTilesToCrack(contactPoints[0].scl(Box2DWorld.BOX_TO_WORLD), contactPoints[1].scl(Box2DWorld.BOX_TO_WORLD),
                                playerPhysMap.get(Player.this).body.getPosition().scl(Box2DWorld.BOX_TO_WORLD), false);
                    }

                }
            }

            @Override
            public void endContact(Entity e, Contact contact) {
                if(e instanceof Map) {
                    // Not colliding with ground anymore!
                    playerMap.get(Player.this).groundContactPoint.removeIndex(0);
                }
            }

            @Override
            public void preSolve(Entity e, Contact contact) {

            }

            @Override
            public void postSolve(Entity e, Contact contact, ContactImpulse impulse) {

            }
        });

        TextureRegionComponent texRegion = new TextureRegionComponent(RoboRunner.assets.get(RoboRunner.assets.PlayerAtlas, TextureAtlas.class).findRegions("idle").first());
        texRegion.offsetY = 10;

        position.pos.set(pos.x, pos.y, 0);
        bounds.bounds.width = PlayerComponent.WIDTH;
        bounds.bounds.height = PlayerComponent.HEIGHT;

        this.add(physics);
        this.add(position);
        this.add(bounds);
        this.add(player);
        this.add(texRegion);
    }

}
