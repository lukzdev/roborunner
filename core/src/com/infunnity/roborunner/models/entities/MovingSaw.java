package com.infunnity.roborunner.models.entities;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.components.entityComponents.SawComponent;
import com.infunnity.roborunner.models.Box2DWorld;

/**
 * Created by Lukasz on 2014-09-18.
 */
public class MovingSaw extends Entity {

    public MovingSaw(float x, float y, float bladeRotationSpeed, float radius, Array<Vector2> waypoints, FloatArray times, Box2DWorld box2DWorld, int layerNum) {
        super();

        Body body = box2DWorld.getBodyBuilder()
                //.mass(1f)
                .fixture(box2DWorld.getFixtureDefBuilder()
                        .circleShape(radius)
                        .density(1f)
                        .friction(1f)
                        .restitution(0f)
                        .maskBits(Box2DWorld.OBSTACLE_MASK)
                        .categoryBits(Box2DWorld.CATEGORY.OBSTACLE)
                        .build())
                .position(x, y)
//                .angle(0.12f)
                .type(BodyDef.BodyType.KinematicBody)
                .userData(this)
                .build();

        body.setAngularVelocity(bladeRotationSpeed * 10);

        TransformComponent position = new TransformComponent();
        BoundsComponent bounds = new BoundsComponent();
        PhysicsComponent physics = new PhysicsComponent(body, new PhysicsComponent.PhysicsListener() {
            @Override
            public void beginContact(Entity e, Contact contact) {

            }

            @Override
            public void endContact(Entity e, Contact contact) {

            }

            @Override
            public void preSolve(Entity e, Contact contact) {

            }

            @Override
            public void postSolve(Entity e, Contact contact, ContactImpulse impulse) {

            }
        });

        TextureRegionComponent texRegion = new TextureRegionComponent(RoboRunner.assets.get(RoboRunner.assets.ObstacleAtlas, TextureAtlas.class).findRegion("saw-" + Math.round(radius*2)));
        texRegion.layer = layerNum;

        position.pos.set(x, y, 0);
        bounds.bounds.width = Math.round(radius*2);
        bounds.bounds.height = Math.round(radius*2);

        WaypointComponent waypointComponent = new WaypointComponent();
        waypointComponent.waypoints = waypoints;
        waypointComponent.waypointTimes = times;

        SawComponent sawComponent = new SawComponent();
        sawComponent.spriteRotationSpeed = bladeRotationSpeed;

        this.add(physics);
        this.add(sawComponent);
        this.add(waypointComponent);
        this.add(position);
        this.add(bounds);
        this.add(texRegion);

    }
}
