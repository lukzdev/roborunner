package com.infunnity.roborunner.models;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.*;
import com.infunnity.roborunner.components.entityComponents.PlayerComponent;
import com.infunnity.roborunner.controllers.ControllerInputHandler;
import com.infunnity.roborunner.controllers.DebugInputHandler;
import com.infunnity.roborunner.controllers.PlayerInputHandler;
import com.infunnity.roborunner.models.entities.Player;
import com.infunnity.roborunner.models.entities.PlayerOnGroundSensor;
import com.infunnity.roborunner.models.entities.PlayerOnWallSensor;
import com.infunnity.roborunner.systems.*;
import com.infunnity.roborunner.systems.entitySystems.PlayerSystem;
import com.infunnity.roborunner.systems.entitySystems.RotatingSawSystem;
import com.infunnity.roborunner.systems.entitySystems.SawSystem;
import com.infunnity.roborunner.systems.entitySystems.SpikeTriggerSystem;
import com.infunnity.roborunner.utils.DebugBackgroundRenderer;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class GameWorld implements ContactListener {

    private Engine entEngine;
    private RoboRunner game;

    private Box2DWorld box2DWorld;
    private Map map;
    private Player player;

    private Stage stage;

    private DebugBackgroundRenderer dbr;

    private Array<Controller> controllerArray = new Array<Controller>();

    private SoundManager soundManager;

    private ComponentMapper<MapComponent> mapMap = ComponentMapper.getFor(MapComponent.class);

    public GameWorld(RoboRunner game, Engine entEngine) {
        this.game = game;
        this.entEngine = entEngine;

        this.box2DWorld = new Box2DWorld(new Vector2(0, -12));
        this.box2DWorld.getWorld().setContactListener(this);

        this.map = new Map(box2DWorld, entEngine);
        this.player = ComponentMapper.getFor(MapComponent.class).get(map).player;

        stage = new Stage(new ScreenViewport());
        Entity guiEntity = new Entity();
        GuiComponent guiComp = new GuiComponent(stage);
        guiEntity.add(guiComp);

//        Controllers.addListener(new ControllerInputHandler(player));

        PlayerInputHandler playerInputHandler = new PlayerInputHandler(player);
        Gdx.input.setInputProcessor(new InputMultiplexer(new DebugInputHandler(game, this), playerInputHandler));

        Entity camEntity = new Entity();
        CameraComponent cameraComponent = new CameraComponent(player);
        cameraComponent.bounding.set(mapMap.get(map).mapBounds);
        cameraComponent.camera.zoom = mapMap.get(map).zoom;
        camEntity.add(cameraComponent);
        this.entEngine.addEntity(camEntity);

        // Ground sensors
        PlayerOnGroundSensor playerOnGroundSensor = new PlayerOnGroundSensor(0, 0, PlayerComponent.WIDTH / 2 * 0.9f,
                50 / 2 * 0.2f, player, box2DWorld, entEngine);

        PlayerOnWallSensor playerLeftWallSensor = new PlayerOnWallSensor(0, 0, PlayerComponent.WIDTH / 2 * 0.2f,
                PlayerComponent.HEIGHT / 2 * 0.9f, player, box2DWorld, true);

        PlayerOnWallSensor playerRightWallSensor = new PlayerOnWallSensor(0, 0, PlayerComponent.WIDTH / 2 * 0.2f,
                PlayerComponent.HEIGHT / 2 * 0.9f, player, box2DWorld, false);

        this.entEngine.addEntity(map);
        this.entEngine.addEntity(player);
        this.entEngine.addEntity(playerOnGroundSensor);
        this.entEngine.addEntity(playerLeftWallSensor);
        this.entEngine.addEntity(playerRightWallSensor);
        this.entEngine.addEntity(guiEntity);

        this.entEngine.addSystem(new Box2DTransformSystem(Box2DWorld.BOX_TO_WORLD, -10));
        this.entEngine.addSystem(new MapSystem());
        this.entEngine.addSystem(new WaypointSystem());
        this.entEngine.addSystem(new SpikeTriggerSystem());
        this.entEngine.addSystem(new SawSystem());
        this.entEngine.addSystem(new RotatingSawSystem());
        this.entEngine.addSystem(new CameraSystem());
        this.entEngine.addSystem(new GuiSystem(player, playerInputHandler));
        this.entEngine.addSystem(new OnGroundSensorSystem());
        this.entEngine.addSystem(new OnWallSensorSystem());
        this.entEngine.addSystem(new PlayerSystem());

        this.entEngine.addSystem(new ParticleSystem());
        this.entEngine.addSystem(new BodyDeactivatorSystem());
        this.entEngine.addSystem(new ResetSytem());
        this.entEngine.addSystem(new RenderingSystem(map, guiEntity, 10, camEntity));

        switchBox2DDebug();

        this.soundManager = SoundManager.getInstance();
        soundManager.playMusic();
    }

    private ComponentMapper<CameraComponent> camMap = ComponentMapper.getFor(CameraComponent.class);
    Box2DDebugRenderSystem box2DDebug = null;
    public void switchBox2DDebug() {
        if(RoboRunner.DEBUG) {
            if(box2DDebug == null) {
                box2DDebug = new Box2DDebugRenderSystem(box2DWorld.getWorld(),
                        camMap.get(entEngine.getEntitiesFor(Family.getFor(CameraComponent.class)).first()).camera,
                        box2DWorld.BOX_TO_WORLD, 100);
            }
            entEngine.addSystem(box2DDebug);

        } else {
            if(box2DDebug != null)
                entEngine.removeSystem(box2DDebug);
        }
    }

    public void update(float dt) {
        for (Controller controller : Controllers.getControllers()) {
            if(!controllerArray.contains(controller, true)) {
                System.out.println("Connecting: " + controller.getName());
                controllerArray.add(controller);
                controller.addListener(new ControllerInputHandler(player));
            }
        }

        box2DWorld.update(dt);
    }

    private ComponentMapper<PhysicsComponent> physMap = ComponentMapper.getFor(PhysicsComponent.class);
    private ComponentMapper<PlayerComponent> playerMap = ComponentMapper.getFor(PlayerComponent.class);

    @Override
    public void beginContact(Contact contact) {
        Object userDataA = contact.getFixtureA().getBody().getUserData();
        Object userDataB = contact.getFixtureB().getBody().getUserData();

        Entity ent1 = (Entity)userDataA;
        Entity ent2 = (Entity)userDataB;

        PhysicsComponent pc1 = physMap.get(ent1);
        PhysicsComponent pc2 = physMap.get(ent2);

        if(pc1 != null)
            pc1.physicsListener.beginContact(ent2, contact);

        if(pc2 != null)
            pc2.physicsListener.beginContact(ent1, contact);

//        if(userDataA instanceof Entity) {
//            PhysicsComponent pc = null;
//
//            Entity ent1 = (Entity)userDataA;
//            pc = physMap.get(ent1);
//
//            if(pc != null) {
//                pc.physicsListener.beginContact(ent);
//            }
//
//        }

//        System.out.println("koliduje");

//        Entity ent1 = (Entity)contact.getFixtureA().getBody().getUserData();
//        Entity ent2 = (Entity)contact.getFixtureB().getBody().getUserData();


    }

    @Override
    public void endContact(Contact contact) {
        Object userDataA = contact.getFixtureA().getBody().getUserData();
        Object userDataB = contact.getFixtureB().getBody().getUserData();

        Entity ent1 = (Entity)userDataA;
        Entity ent2 = (Entity)userDataB;

        PhysicsComponent pc1 = physMap.get(ent1);
        PhysicsComponent pc2 = physMap.get(ent2);

        if(pc1 != null)
            pc1.physicsListener.endContact(ent2, contact);

        if(pc2 != null)
            pc2.physicsListener.endContact(ent1, contact);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Object userDataA = contact.getFixtureA().getBody().getUserData();
        Object userDataB = contact.getFixtureB().getBody().getUserData();

        Entity ent1 = (Entity)userDataA;
        Entity ent2 = (Entity)userDataB;

        PhysicsComponent pc1 = physMap.get(ent1);
        PhysicsComponent pc2 = physMap.get(ent2);

        if(pc1 != null)
            pc1.physicsListener.postSolve(ent2, contact, impulse);

        if(pc2 != null)
            pc2.physicsListener.postSolve(ent1, contact, impulse);
    }

    public void dispose() {
        stage.dispose();
    }
}
