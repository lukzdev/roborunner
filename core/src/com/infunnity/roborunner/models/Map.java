package com.infunnity.roborunner.models;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.infunnity.roborunner.RoboRunner;
import com.infunnity.roborunner.components.MapComponent;
import com.infunnity.roborunner.models.entities.Player;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class Map extends Entity {

    private Box2DWorld box2DWorld;
//    private Player player;

    private TiledMap map;

    Array<StaticTiledMapTile> tinyCracks = new Array<StaticTiledMapTile>(); // For running over terrain
    Array<StaticTiledMapTile> smallCracks = new Array<StaticTiledMapTile>();
    Array<StaticTiledMapTile> mediumCracks = new Array<StaticTiledMapTile>();
    Array<StaticTiledMapTile> bigCracks = new Array<StaticTiledMapTile>();

    private ComponentMapper<MapComponent> mapMapper = ComponentMapper.getFor(MapComponent.class);

    public Map(Box2DWorld box2DWorld, Engine engine) {
        this.box2DWorld = box2DWorld;

        // Loading map
        map = RoboRunner.assets.get(RoboRunner.assets.Map4, TiledMap.class);

        MapComponent mapComponent = new MapComponent();
        mapComponent.player = new Player(MapProcessor.getStartPos(map), box2DWorld, engine);
        add(mapComponent);

        MapProcessor.processProperties(this, map);
        MapProcessor.processLayers(this);

        // Make layer for cracks
        mapMapper.get(Map.this).crackTiles = new TiledMapTileLayer(mapMapper.get(Map.this).groundTiles.getWidth(),
                mapMapper.get(Map.this).groundTiles.getHeight(), (int)mapMapper.get(Map.this).groundTiles.getTileWidth(),
                (int)mapMapper.get(Map.this).groundTiles.getTileHeight());

        MapProcessor.createObstacleObjects(this, mapMapper.get(Map.this).backgroundObjects, box2DWorld, engine, -1);
        MapProcessor.createObstacleObjects(this, mapMapper.get(Map.this).foregroundObjects, box2DWorld, engine, 1);

        MapProcessor.createGroundObjects(this, mapMapper.get(Map.this).groundObjects, box2DWorld);

        smallCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-small-1")));
        smallCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-small-2")));
        mediumCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-medium-1")));
        mediumCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-medium-2")));
        bigCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-big-1")));
        bigCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-big-2")));
        // Just for running over terrain
        tinyCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-tiny-1")));
        tinyCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-tiny-2")));
        tinyCracks.add(new StaticTiledMapTile(RoboRunner.assets.get(RoboRunner.assets.SpaceFactoryAtlas, TextureAtlas.class).findRegion("crack-tiny-2")));


    }

    private Vector2 tileDirection = new Vector2(0,0);
    private Vector2 firstTilePos = new Vector2(0,0);
    private Vector2 secondTilePos = new Vector2(0,0);
    public void findTilesToCrack(Vector2 point1, Vector2 point2, Vector2 playerPos, boolean tiny) {
        // Get direction!
        tileDirection.x = ((point1.x - playerPos.x) + (point2.x - playerPos.x));
        tileDirection.y = ((point1.y - playerPos.y) + (point2.y - playerPos.y));
        tileDirection.nor().scl(mapMapper.get(Map.this).tileWidth / 2);

        // Get first tile position
        firstTilePos.x = (int)((point1.x + tileDirection.x) / mapMapper.get(Map.this).tileWidth);
        firstTilePos.y = (int)((point1.y + tileDirection.y) / mapMapper.get(Map.this).tileWidth);

        // Get second tile position
        secondTilePos.x = (int)((point2.x + tileDirection.x) / mapMapper.get(Map.this).tileWidth);
        secondTilePos.y = (int)((point2.y + tileDirection.y) / mapMapper.get(Map.this).tileWidth);

        if(firstTilePos.equals(secondTilePos)) {
            crackTile(firstTilePos, tileDirection, tiny);
        } else {
            crackTile(firstTilePos, tileDirection, tiny);
            crackTile(secondTilePos, tileDirection, tiny);
        }
    }

    private Vector2 newDirection = new Vector2(0, 0);
    public void crackTile(Vector2 tilePos, Vector2 direction, boolean tiny) {
        TiledMapTileLayer.Cell cell = mapMapper.get(Map.this).crackTiles.getCell((int)tilePos.x, (int)tilePos.y);

        if(cell == null) {
            // Make new cell!
            TiledMapTileLayer.Cell newCell = new TiledMapTileLayer.Cell();

            // Weird things to calculate angle
            newDirection.set(direction).scl(-1);
            newDirection.setAngle(newDirection.angle() - 90);
            newCell.setRotation(Math.round((newDirection.angle() / 90)));

            // Tile
            if(!tiny) {
                newCell.setTile(smallCracks.random());
            } else {
                newCell.setTile(tinyCracks.random());
            }

            newCell.setFlipHorizontally(MathUtils.randomBoolean());
            mapMapper.get(Map.this).crackTiles.setCell((int)tilePos.x, (int)tilePos.y, newCell);
        } else {
            // Don't increment size on walk
            if(tiny)
                return;

            // Modify existing cell!
            if(smallCracks.contains((StaticTiledMapTile)cell.getTile(), true) || tinyCracks.contains((StaticTiledMapTile)cell.getTile(), true)) {
                cell.setTile(mediumCracks.random());
                cell.setFlipHorizontally(MathUtils.randomBoolean());
            } else if(mediumCracks.contains((StaticTiledMapTile)cell.getTile(), true)) {
                cell.setTile(bigCracks.random());
                cell.setFlipHorizontally(MathUtils.randomBoolean());
            } else {
                return;
            }
        }

    }

    public TiledMap getMap() {
        return map;
    }
}
