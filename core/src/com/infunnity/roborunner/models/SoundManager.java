package com.infunnity.roborunner.models;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.infunnity.roborunner.RoboRunner;

/**
 * Created by Lukasz on 2014-09-20.
 */
public class SoundManager {

    private static SoundManager instance;

    // Music
    private Music edgeOfSolance;

    // Sounds
    private Sound landSound;


    private SoundManager() {
        edgeOfSolance = RoboRunner.assets.get(RoboRunner.assets.MusicEdgeOfSolance, Music.class);
        landSound = RoboRunner.assets.get(RoboRunner.assets.LandSound, Sound.class);

    }

    public static SoundManager getInstance() {
        if(instance == null) {
            instance = new SoundManager();
        }

        return instance;
    }

    public void playMusic() {
        edgeOfSolance.setLooping(true);
        edgeOfSolance.setVolume(0.4f);
        edgeOfSolance.play();
    }

    public void playLandSound(float volume) {
        landSound.play(volume);
    }
}
