package com.infunnity.roborunner.models;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.*;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.infunnity.roborunner.components.MapComponent;
import com.infunnity.roborunner.models.entities.MovingSaw;
import com.infunnity.roborunner.models.entities.RotatingSaw;
import com.infunnity.roborunner.models.entities.Saw;
import com.infunnity.roborunner.models.entities.TriggerSpikes;
import com.infunnity.roborunner.utils.BodyBuilder;
import com.infunnity.roborunner.utils.dermetfan.GeometryUtils;

import java.util.Iterator;

/**
 * Created by Lukasz on 2014-08-06.
 */
public class MapProcessor {

    public static final String BACKGROUND_TILES = "-2_tiles_background";
    public static final String BACKGROUND_OBJECTS = "-1_objects_before_ground";
    public static final String GROUND_TILES = "0_tiles_ground";
    public static final String GROUND_OBJECTS = "0_objects_ground";
    public static final String FOREGROUND_OBJECTS = "1_objects_after_ground";
    public static final String FOREGROUND_TILES = "2_tiles_foreground";

    private static ComponentMapper<MapComponent> mapMapper = ComponentMapper.getFor(MapComponent.class);

    public static void processLayers(Map map) {
        for(MapLayer layer : map.getMap().getLayers()) {
            Color tempColor = new Color(Color.WHITE);

            Iterator properties = layer.getProperties().getKeys();
            while(properties.hasNext()) {
                String tempProp = (String)properties.next();

                if(tempProp.equals("r")) {
                    tempColor.r = Float.valueOf((String)layer.getProperties().get("r"));
                } else if (tempProp.equals("g")) {
                    tempColor.g = Float.valueOf((String) layer.getProperties().get("g"));
                } else if (tempProp.equals("b")) {
                    tempColor.b = Float.valueOf((String) layer.getProperties().get("b"));
                } else if (tempProp.equals("a")) {
                    tempColor.a = Float.valueOf((String) layer.getProperties().get("a"));
                }
            }

            if(layer.getName().equals(BACKGROUND_TILES)) {
                mapMapper.get(map).backgroundTiles = (TiledMapTileLayer)layer;
                mapMapper.get(map).backgroundTilesColor = tempColor;
            } else if(layer.getName().equals(BACKGROUND_OBJECTS)) {
                mapMapper.get(map).backgroundObjects = layer;
                mapMapper.get(map).backgroundObjectsColor = tempColor;
            } else if(layer.getName().equals(GROUND_TILES)) {
                TiledMapTileLayer mapLayer = (TiledMapTileLayer)layer;
                mapMapper.get(map).groundTiles = mapLayer;
                mapMapper.get(map).groundTilesColor = tempColor;
            } else if(layer.getName().equals(GROUND_OBJECTS)) {
                mapMapper.get(map).groundObjects = layer;
                mapMapper.get(map).groundObjectsColor = tempColor;
            } else if(layer.getName().equals(FOREGROUND_TILES)) {
                TiledMapTileLayer mapLayer = (TiledMapTileLayer)layer;
                mapMapper.get(map).foregroundTiles = mapLayer;
                mapMapper.get(map).foregroundTilesColor = tempColor;
            } else if(layer.getName().equals(FOREGROUND_OBJECTS)) {
                mapMapper.get(map).foregroundObjects = layer;
                mapMapper.get(map).foregroundObjectsColor = tempColor;
            }
        }
    }

    public static void createGroundObjects(Map map, MapLayer layer, Box2DWorld world) {
        for(MapObject object : layer.getObjects()) {

            if(object instanceof PolylineMapObject) {
                polylineGround(object, map, world);
            } else if(object instanceof RectangleMapObject) {
                rectangleGround(object, map, world);
            } else if(object instanceof PolygonMapObject) {
                polygonGround(object, map, world);
            }

        }
    }

    public static void polylineGround(MapObject object, Map map, Box2DWorld world) {
        PolylineMapObject polyObject = (PolylineMapObject)object;
        Polyline polyline = new Polyline(polyObject.getPolyline().getTransformedVertices());
        polyline.setScale(world.WORLD_TO_BOX, world.WORLD_TO_BOX);

        world.getBodyBuilder()
                .fixture(world.getFixtureDefBuilder()
                        .chainShape(polyline.getTransformedVertices()))
                        //.position(RoboRunner.TARGET_WIDTH / 2, 5)
                .type(BodyDef.BodyType.StaticBody)
                .userData(map)
                .build();
    }

    public static void polygonGround(MapObject object, Map map, Box2DWorld world) {
        PolygonMapObject polyObject = (PolygonMapObject)object;
        Polygon polygon = new Polygon(polyObject.getPolygon().getTransformedVertices());
        polygon.setScale(world.WORLD_TO_BOX, world.WORLD_TO_BOX);

        Polygon[] convexPolygons = GeometryUtils.decompose(polygon);
        BodyBuilder bodyBuilder = world.getBodyBuilder();

        for(Polygon convexPolygon : convexPolygons) {
            bodyBuilder.fixture(world.getFixtureDefBuilder().polygonShape(convexPolygon.getTransformedVertices()));
        }

//        world.getBodyBuilder()
//                .fixture(world.getFixtureDefBuilder()
//                        .polygonShape(polygon.getTransformedVertices()))
                        //.position(RoboRunner.TARGET_WIDTH / 2, 5)
        bodyBuilder.type(BodyDef.BodyType.StaticBody)
                .userData(map)
                .build();
    }

    public static void rectangleGround(MapObject object, Map map, Box2DWorld world) {
        RectangleMapObject polyObject = (RectangleMapObject)object;
        Rectangle rectangle = new Rectangle(polyObject.getRectangle());
//        rectangle.setScale(world.WORLD_TO_BOX, world.WORLD_TO_BOX);
        world.getBodyBuilder()
                .fixture(world.getFixtureDefBuilder()
                        .boxShape(rectangle.getWidth() / 2, rectangle.getHeight() / 2)
                        .build())
                .position(rectangle.getX() + rectangle.getWidth() / 2, rectangle.getY() + rectangle.getHeight() / 2)
                .type(BodyDef.BodyType.StaticBody)
                .userData(map)
                .build();
    }


    public static void processProperties(Map map, TiledMap tileMap) {
        // Process map bounds
        Rectangle mapBounds = ComponentMapper.getFor(MapComponent.class).get(map).mapBounds;

        mapBounds.x = 0;
        mapBounds.y = 0;
        mapBounds.width = (Integer)tileMap.getProperties().get("width") * (Integer)tileMap.getProperties().get("tilewidth");
        mapBounds.height = (Integer)tileMap.getProperties().get("height") * (Integer)tileMap.getProperties().get("tileheight");

        mapMapper.get(map).tileWidth = (Integer)tileMap.getProperties().get("tileheight");

        if(tileMap.getProperties().containsKey("zoom")) {
            mapMapper.get(map).zoom = Float.valueOf((String)tileMap.getProperties().get("zoom"));
        }
    }


    public static Vector2 getStartPos(TiledMap map) {
        Object startX = map.getProperties().get("startX");
        Object startY = map.getProperties().get("startY");

        Vector2 startPos = new Vector2(0, 0);

        if(startX != null && startY != null) {
            startPos.set(Float.valueOf((String)startX), Float.valueOf((String)startY));
        }
        startPos.y = (Integer)map.getProperties().get("height") - startPos.y;

        startPos.x *= (Integer)map.getProperties().get("tilewidth");
        startPos.y *= (Integer)map.getProperties().get("tileheight");

        startPos.x += (Integer)map.getProperties().get("tilewidth") / 2;
        startPos.y -= (Integer)map.getProperties().get("tileheight") / 2;

        return startPos;
    }



    public static void createObstacleObjects(Map map, MapLayer layer, Box2DWorld world, Engine engine, int layerNum) {
        for(MapObject object : layer.getObjects()) {

            if(object.getProperties().get("type").equals("saw")) {
                createSaw(object, world, engine, layerNum);
            } else if(object.getProperties().get("type").equals("rotating-saw")) {
                createRotatingSaw(object, map, world, engine, layerNum);
            } else if(object.getProperties().get("type").equals("moving-saw")) {
                createMovingSaw(object, map, world, engine, layerNum);
            } else if(object.getProperties().get("type").equals("trigger-spikes")) {
                createTriggerSpikes(object, map, world, engine, layerNum);
            }

        }
    }



    public static void createSaw(MapObject object, Box2DWorld world, Engine engine, int layerNum) {
        EllipseMapObject obj = (EllipseMapObject)object;

        float speed = 1;
        if(obj.getProperties().containsKey("speed")) {
            speed = Float.valueOf((String)obj.getProperties().get("speed"));
        }

        Saw saw = new Saw(obj.getEllipse().x + obj.getEllipse().width / 2, obj.getEllipse().y + obj.getEllipse().width / 2, speed, obj.getEllipse().width / 2, world, layerNum);
        engine.addEntity(saw);
    }

    public static void createRotatingSaw(MapObject object, Map map, Box2DWorld world, Engine engine, int layerNum) {
        EllipseMapObject obj = (EllipseMapObject)object;

        float rotationRadius = 0;
        if(obj.getProperties().containsKey("radius")) {
            rotationRadius = Float.valueOf((String)obj.getProperties().get("radius")) * mapMapper.get(map).tileWidth;
        }

        float rotationSpeed = 1;
        if(obj.getProperties().containsKey("speed")) {
            rotationSpeed = Float.valueOf((String)obj.getProperties().get("speed"));
        }

        float speed = 1;
        if(obj.getProperties().containsKey("speed")) {
            speed = Float.valueOf((String)obj.getProperties().get("speed"));
        }

        RotatingSaw saw = new RotatingSaw(obj.getEllipse().x + obj.getEllipse().width / 2, obj.getEllipse().y + obj.getEllipse().width / 2, speed, rotationRadius, rotationSpeed, obj.getEllipse().width / 2, world, layerNum);
        engine.addEntity(saw);
    }

    public static void createMovingSaw(MapObject object, Map map, Box2DWorld world, Engine engine, int layerNum) {
        EllipseMapObject obj = (EllipseMapObject)object;

        Array<Vector2> waypoints = new Array<Vector2>();
        // add start pos
        waypoints.add(new Vector2(obj.getEllipse().x + obj.getEllipse().width / 2, obj.getEllipse().y + obj.getEllipse().width / 2));

        if(obj.getProperties().containsKey("waypoints")) {
            String waypointsString = (String)obj.getProperties().get("waypoints");
            String[] waypointsPoints = waypointsString.split(",");
            for(int i = 0; i < waypointsPoints.length - 1; i+= 2) {
                Vector2 pos = new Vector2(Float.valueOf(waypointsPoints[i]), Float.valueOf(waypointsPoints[i+1]));
                Vector2 worldPos = mapToWorldPos(pos, map.getMap(), true);
                waypoints.add(worldPos);
            }
        } else {
            return;
        }

        FloatArray times = new FloatArray();
        if(obj.getProperties().containsKey("times")) {
            String timesString = (String)obj.getProperties().get("times");
            String[] timesSingle = timesString.split(",");
            for(int i = 0; i < timesSingle.length; i++) {
                times.add(Float.valueOf(timesSingle[i]));
            }
        } else {
            return;
        }

        if(waypoints.size - 1 != times.size)
            return;

        float speed = 1;
        if(obj.getProperties().containsKey("speed")) {
            speed = Float.valueOf((String)obj.getProperties().get("speed"));
        }

        MovingSaw saw = new MovingSaw(obj.getEllipse().x + obj.getEllipse().width / 2, obj.getEllipse().y + obj.getEllipse().width / 2, speed, obj.getEllipse().width / 2, waypoints, times, world, layerNum);
        engine.addEntity(saw);

    }

    public static void createTriggerSpikes(MapObject object, Map map, Box2DWorld world, Engine engine, int layerNum) {
        RectangleMapObject spikesMapObj = (RectangleMapObject)object;

        if(!spikesMapObj.getProperties().containsKey("direction")) {
            return;
        }

        String directionStr = (String)spikesMapObj.getProperties().get("direction");
        TriggerSpikes.SpikesDirection direction = null;
        if(directionStr.equals("up")) {
            direction = TriggerSpikes.SpikesDirection.UP;
        } else if(directionStr.equals("down")) {
            direction = TriggerSpikes.SpikesDirection.DOWN;
        } else if(directionStr.equals("left")) {
            direction = TriggerSpikes.SpikesDirection.LEFT;
        } else if(directionStr.equals("right")) {
            direction = TriggerSpikes.SpikesDirection.RIGHT;
        }

        TriggerSpikes spikes = null;
        switch(direction) {
            case UP:
            case DOWN:
                spikes = new TriggerSpikes(spikesMapObj.getRectangle().x, spikesMapObj.getRectangle().y, spikesMapObj.getRectangle().width, direction, world);
                break;
            case LEFT:
            case RIGHT:
                spikes = new TriggerSpikes(spikesMapObj.getRectangle().x, spikesMapObj.getRectangle().y, spikesMapObj.getRectangle().height, direction, world);
                break;
        }

        engine.addEntity(spikes);
    }

    public static Vector2 mapToWorldPos(Vector2 position, TiledMap map, boolean centerPos) {
        Vector2 worldPos = new Vector2(position);

        worldPos.y = (Integer)map.getProperties().get("height") - (worldPos.y + 1);

        worldPos.x *= (Integer)map.getProperties().get("tilewidth");
        worldPos.y *= (Integer)map.getProperties().get("tilewidth");

        if(centerPos) {
            worldPos.x += (Integer)map.getProperties().get("tilewidth") / 2;
            worldPos.y += (Integer)map.getProperties().get("tilewidth") / 2;
        }

        return worldPos;
    }

}
