package com.infunnity.roborunner.models;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.infunnity.roborunner.utils.BodyBuilder;
import com.infunnity.roborunner.utils.FixtureDefBuilder;

/**
 * Created by Lukasz on 2014-08-04.
 */
public class Box2DWorld {

    /*
	 * Statics for calculation pixel to box2d metrics and vice versa
	 */
    public static final float WORLD_TO_BOX = 0.01f;
    public static final float BOX_TO_WORLD = 100f;

    public final static short OBSTACLE_MASK = Box2DWorld.CATEGORY.PLAYER | Box2DWorld.CATEGORY.PLAYER_PARTICLES;
    public final static short PARTICLE_MASK = Box2DWorld.CATEGORY.GROUND | Box2DWorld.CATEGORY.OBSTACLE | Box2DWorld.CATEGORY.PLAYER_PARTICLES;
    public final static short SENSOR_MASK = Box2DWorld.CATEGORY.GROUND;

    public final static class CATEGORY {
        public final static short GROUND = 0x0001;
        public final static short PLAYER = 0x0002;
        public final static short SENSOR = 0x0004;
        public final static short OBSTACLE = 0x0008;
        public final static short PLAYER_PARTICLES = 0x0010;
    };

    private World world;
//    private Box2DDebugRenderer renderer;

    private FixtureDefBuilder fixtureDefBuilder;
    private BodyBuilder bodyBuilder;

    public Box2DWorld(Vector2 gravity) {
//        World.setVelocityThreshold(WORLD_TO_BOX);
        world = new World(gravity, true);
//        renderer = new Box2DDebugRenderer();

        bodyBuilder = new BodyBuilder(world);
        fixtureDefBuilder = new FixtureDefBuilder();
    }

    public void update(float dt) {
        world.step(dt, 6, 2);
        //sweepDeadBodies();

    }

    /*
	 * Bodies should be removed after world step to prevent simulation crash
	 */

	/*public void sweepDeadBodies() {
		getWorld().getBodies(bodies);
		for (Iterator<Body> iter = bodies.iterator(); iter.hasNext();) {
			Body body = iter.next();
			if (body != null && (body.getUserData() instanceof Player)) {
				Player data = (Player) body.getUserData();
				if (data.isFlaggedForDelete) {
					getWorld().destroyBody(body);
					body.setUserData(null);
					body = null;
				}
			}
		}
	}*/

    /*
	 * Render box2d debug
    */
//    public void render(Camera cam) {
//        renderer.render(world, cam.combined.cpy().scl(BOX_TO_WORLD));
//    }

    public World getWorld() {
        return world;
    }

    public BodyBuilder getBodyBuilder() {
        return bodyBuilder;
    }

    public FixtureDefBuilder getFixtureDefBuilder() {
        return fixtureDefBuilder;
    }
}
