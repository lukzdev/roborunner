Untitled
- Delay -
active: false
- Duration - 
lowMin: 64.0
lowMax: 64.0
- Count - 
min: 0
max: 20
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 1000.0
highMax: 1000.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 1.0
scaling2: 0.3
timelineCount: 3
timeline0: 0.0
timeline1: 0.66
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 2.0
highMax: 2.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 12.0
highMax: 120.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 180.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -100.0
highMax: -100.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.0
scaling2: 1.0
scaling3: 1.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.33561644
timeline2: 0.999
timeline3: 1.0
- Tint - 
colorsCount: 9
colors0: 0.99215686
colors1: 0.99215686
colors2: 0.99215686
colors3: 1.0
colors4: 0.99607843
colors5: 0.0
colors6: 1.0
colors7: 0.7882353
colors8: 0.03137255
timelineCount: 3
timeline0: 0.0
timeline1: 0.49717513
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.75
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2
timeline2: 0.8
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
premultipliedAlpha: false
- Image Path -
D:\Projekty\android\roborunner\assets\graphics\player\spark_particle.png
