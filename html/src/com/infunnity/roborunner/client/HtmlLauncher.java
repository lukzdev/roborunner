package com.infunnity.roborunner.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.infunnity.roborunner.RoboRunner;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {

//            GwtApplicationConfiguration(480, 320)
            GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(990, 557);
            cfg.fps = 60;
            cfg.canvasId = "game";

            return cfg;
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new RoboRunner();
        }
}